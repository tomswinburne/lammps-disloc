# LAMMPS package for dislocation identification 

> **NOTE: This repository is for internal use only! Do not use the code from this repository without approval from the author.**

This repository hosts the source code of a [LAMMPS](http://lammps.sandia.gov) extension package that can identify dislocation 
defects in molecular dynamics simulations of crystals. Currently, the package supports only
simulation setups representing fully dense, single crystalline and 3-d periodic crystals (no inner or outer surfaces).
The algorithm for identifying dislocation lines is based on the approach described in this paper:

[A. Stukowski: _A triangulation-based method to identify dislocations in atomistic models_  
Journal of the Mechanics and Physics of Solids 70, 314-319 (2014)](https://doi.org/10.1016/j.jmps.2014.06.009)

## Installation

To install the package, clone this Git repository into a subdirectory named ``USER-DISLOC`` inside the ``src/`` directory 
of your local LAMMPS installation:

```
cd <lammps-path>/src/
git clone https://gitlab.com/stuko/lammps-disloc.git USER-DISLOC
```

The module requires the NetCDF library. 

NetCDF should be available as a pre-installed system library on most machines. If the helper program ``nc-config`` is in your path, then LAMMPS will use it to automatically discover the NetCDF library and header file locations. If not, see 
[``<LAMMPS>/lib/netcdf/README``](https://github.com/lammps/lammps/blob/master/lib/netcdf/README) for further instructions.

Source code for the [Geogram library](http://alice.loria.fr/software/geogram/doc/html/index.html) is included in this repository. Note that Geogram only supports the GCC, Clang, and Intel 
compilers. That means LAMMPS must also be built with one of these compilers. The IBM XL compiler is currently not supported by Geogram.
Furthermore, Geogram makes use of some C++11 language constructs. Thus, LAMMPS must be built with the ``-std=c++11`` compiler switch.

To activate the extension package, run 
```
make yes-user-disloc
``` 
After this, LAMMPS needs to be recompiled, [typically by running ``make <machine-name>``](http://lammps.sandia.gov/doc/Section_start.html#making-lammps).

## Usage

### Setting the required ghost cutoff distance

In order to build a Delaunay tessellation that is consistent across neighboring processor sub-domains, the algorithm requires sufficient overlap between the processor domains. In LAMMPS the so-called *ghost cutoff distance* controls the amount of overlap, i.e. the distance up to which ghost atoms are generated around each processor sub-domain. Ghost atoms may move once they have been created, and that means the effective distance up to which ghost atom information is trustworthy gradually declines. The maximum distance any ghost atom may move until a regeneration of the entire ghost atom layer is triggered is the so-called *neighbor skin distance*. This parameter is 2.0 Angstroms by default and can by changed with the [``neighbor``](http://lammps.sandia.gov/doc/neighbor.html) command of LAMMPS. 

The overlap effectively usable by the disloc fix is given by the ghost cutoff distance minus the neighbor skin distance.

To produce a consistent Delaunay tessellation, the disloc fix requires an effective overlap proportional to the nearest neighbor spacing *d<sub>0</sub>* of the crystal lattice at hand. For BCC Ta with a lattice constant of *a<sub>0</sub>* = 3.31 Angstroms, for example, the interatomic spacing is *d<sub>0</sub>* = sqrt(3)/2·*a<sub>0</sub>* = 2.87 Angstroms. The minimum ghost overlap required by the disloc fix is √8 times this value. Since the effective overlap is the *ghost cutoff distance* reduced by the *neighbor skin distance*, we have to add the skin distance on top of that input value to arrive at the required ghost cutoff distance:

```
(ghost cutoff distance) = sqrt(8) * (interatomic spacing) + (neighbor skin distance)
```

Thus, for tantalum we need to set the ghost cutoff distance to a value of at least (√8·2.87 + 2.0) = 10.1 Angstroms. This must be done using the [comm_modify cutoff](http://lammps.sandia.gov/doc/comm_modify.html) command before fix disloc is invoked, e.g.

```
comm_modify cutoff 10.1
```

Fix disloc will check this value and abort with an error message in case it is too low. Overly large ghost cutoff parameters, on the other hand, may reduce the performance of MD integration steps, as the data size exchanged between processors is increased.

### Defining the fix

The dislocation analysis fix is created with the following LAMMPS command:

```
fix ID group-ID disloc Nevery lattice a0 ...
```

* ID = name you assign to the fix definition
* group-ID = must be ``all``
* Nevery = Identify dislocation defects every this many timesteps
* lattice = *bcc*, *fcc* or *fcc_perfect*
* a0 = lattice parameter of the crystal to be analyzed

**Example:** 
```
fix disloc all disloc 1000 bcc 3.3058 file output/*.disloc.nc orient x 1 1 0 orient y -1 1 0 orient z 0 0 1 debug
```

The fix performs the analysis on timesteps that are multiples of *Nevery* (including timestep 0). Additional keyword arguments can be appended to the fix definition line, in arbitrary order. The supported keywords are described in the following.

### File output

The ``file`` keyword sets the name of the output file to which the fix will dump the extracted dislocation lines and slip surfaces.
The output filename should contain a ``*`` wildcard character, making the fix write one file per analysis snapshot. The ``*`` character get replaced with the current timestep value. 

**Example:** 
```
fix disloc all disloc 1000 bcc 3.3058 file output/*.disloc.nc ...
```

The files written by the dislocation fix are stored in a custom NetCDF-based format and can be opened with [OVITO](http://www.ovito.org) (version 3.0.0-dev101 and newer). The file format is documented in a separate section below.

The ``convention`` keyword selects the sign convention for the dislocations generated by the algorithm. The default is RH/FS. You can request the LH/FS convention (corresponding to a flipping of all Burgers vectors) by specifying a different value for the ``convention`` parameter:
```
fix disloc all disloc 1000 bcc 3.3058 file output/*.disloc.nc convention LH/FS ...
```

To reduce the necessary MPI communication steps and speed up the file I/O in massively-parallel simulations, the ``aggregate_io`` keyword allows to activate an extra data aggregation step, which lumps the data arrays on a fewer number of processors prior to writing them to the output file. An integer parameter controls how many aggregation steps are being performed, reducing the number of processors by half during each step. Note, however, that data aggregation increases the memory requirements on the compute nodes that collect the data. 

**Example:** 
```
fix disloc all disloc 1000 bcc 3.3058 file output/*.disloc.nc aggregate_io 6
```
This sets the number of aggregation steps to 6, letting the code collect the output data on every 64th processor (2^6) prior to writing it to the output file.

### Specifying the crystal orientation

The ``orient`` keyword allows setting the crystal orientation, which will be used by the algorithm to map interatomic vectors to an ideal reference lattice. By default, the fix assumes the [100], [010] and [001] crystal directions are oriented parallel to the x,y,z simulation frame axes. A different orientation is set by using the ``orient`` keyword three times to specify the crystal directions that are parallel to the coordinate axes, e.g.

**Example:** 
```
fix disloc all disloc 1000 bcc 3.3058 orient x 1 1 0 orient y -1 1 0 orient z 0 0 1 ...
``` 

The three lattice vectors must form a right-handed, orthogonal system. Otherwise an error message is generated.

Alternatively, you can couple the fix to a [fix crystalorient](https://gitlab.com/stuko/lammps-crystalorient), which computes the current
orientation of the single crystal on the fly. For that, specify the ID of the crystal orientation fix prefixed with ``c_`` and following the ``orient`` keyword:

**Example:** 
```
compute orient all crystalorient fcc
fix disloc all disloc 1000 fcc 3.619 orient c_orient ...
``` 

### Using time-averaged atomic positions

The ``ave`` keyword tells the dislocation algorithm to work with time-averaged atomic input positions computed by a
``fix ave/atom`` instead of the instantaneous positions. 

**Example:** 
```
compute   unwrapped_pos all property/atom xu yu zu
fix       avg_pos all ave/atom 5 40 1000 c_unwrapped_pos[*]
fix       disloc all disloc 1000 bcc 3.3058 ave avg_pos ...
``` 

Make sure ``fix ave/atom`` computes the time-averaged positions on the same timesteps as ``fix disloc`` needs them.

### Debug output

The ``debug`` keyword activates verbose output to the LAMMPS log file during each analysis step and is provided mainly for debugging purposes. 

### Slip surface analysis

Optionally, the fix can extract the slip surfaces, i.e. the precise paths taken by dislocations through the crystal. Slip analysis mode is activated by appending the ``slip`` keyword, e.g.
```
fix disloc all disloc 1000 bcc 3.3058 slip ...
```

The ``incremental`` keyword option tells the fix to compute the incremental slip surfaces generated by moving dislocations in between two successive invocations of the fix; in this case over windows of 1000 timesteps. Without the ``incremental`` flag, the fix will compute the slip surfaces with respect to the initial timestep of the simulation, i.e. the crystal configuration at the time the fix was first defined.

Slip surface analysis requires an extended overlap between processor sub-domains (ghost cutoff distance, see above). This extra ghost layer thickness must be at least as large as the expected maximum atomic slip magnitude generated over one analysis interval. Typically this ranges between one and two times the Burgers vector magnitude of the crystal. But the ghost layer cushion needed may be much larger when working with extended slip analysis intervals (like in a non-incremental analysis).

Note that the computed slip surfaces are dumped to the output file in addition to the dislocation lines extracted at the current timestep. Thus, it is not necessary to define two separate disloc fixes in order to extract dislocation lines and slip surfaces simultaneously.

## NetCDF file format specification

The LAMMPS fix can dump the extracted dislocation lines and slip surface facets to an binary NetCDF-based output file. The format of this output file is described in the following. 

The format stores a list of *nodes*, each node having three-dimensional coordinates and a unique node ID. Each node ID is composed of four integer numbers, which are derived from the four atom IDs of the corner atoms of the corresponding Delaunay tetrahedron. The code creates a node always in the center of a Delaunay tetrahedron.

The output file furthermore contains a list of dislocation segments. Each segment is defined by two indices into the nodes list. Additionally, each segment is associated with a three-dimensional Burgers vector (given in lattice space).

Note: Some nodes may appear more than once in the nodes list, because multiple processors may decide to emit the same node to the output file. The writing code currently makes no attempt to filter out these extra copies (all having the same node ID). It is the responsibility of the reading code to map all copies to the same physical node. 

##### NetCDF file-level attributes:

| Attribute name      | Data type    | Value/Description|
|---------------------|--------------|------------------|
| Convention          | String       | "FixDisloc"      |
| ConventionVersion   | String       | "1.0"            |
| TimestepNumber      | NC_INT64     | Current LAMMPS integration step |
| TimestepSize        | NC_DOUBLE    | LAMMPS timestep setting |
| TimeInterval        | NC_DOUBLE    | Interval between fix invocations, in simulation time units |
| LatticeStructure    | String       | "fcc" or "bcc" |

##### NetCDF dimensions:

| Dimension name      | Value/Description    |
|---------------------|--------------------------------|
| ``spatial``             | 3 (used for three-dimensional coordinates and vectors) |
| ``pair`` | 2 (used for arrays with two columns) |
| ``nodes`` | Number of nodes |
| ``node_id`` | 4 (Node IDs are composed of four integers) |
| ``dislocations`` | Number of dislocation segments |

##### NetCDF variables:

| Variable name      | Dimensions | Data type | Description    |
|--------------------|------------|-----------|----------------|
| cell_vectors       | ``spatial`` x ``spatial`` | NC_DOUBLE | 3x3 cell matrix |
| cell_origin        | ``spatial`` | NC_DOUBLE | Cell corner coordinates |
| cell_pbc           | ``spatial`` | NC_INT | Periodic boundary flags (0/1) |
| lattice_orientation | ``spatial`` x ``spatial`` | NC_DOUBLE | 3x3 orientation matrix; translates Burgers vectors to the simulation frame |
| nodal_ids          | ``nodes`` x ``node_id`` | NC_TAGINT_TYPE | The IDs of the nodal points |
| nodal_positions    | ``nodes`` x ``spatial`` | NC_FLOAT | Coordinates of nodal points |
| burgers_vectors    | ``dislocations`` x ``spatial`` | NC_FLOAT | Burgers vectors of dislocation segments |
| dislocation_segments | ``dislocations`` x ``pair`` | NC_TAGINT_TYPE | Segment definitions; pairs of indices into the nodes list |


NC_TAGINT_TYPE := NC_INT64 if LAMMPS was compiled with LAMMPS_BIGBIG flag; otherwise NC_INT. 

### Slip facet storage format

The fix additionally writes the extracted slip facets to the NetCDF output file if the ``slip`` keyword is used to activate the slip analysis. The extended format contains the following information.

Slip facets are small polygons made of a variable number of vertex nodes. As for dislocation segments, vertex nodes are stored in the global nodes list defined above. In cases where a slip facet is bounded by a physical dislocation segment, the dislocation segment and the slip facet will share the same vertex nodes. Each slip facet has a slip vector, which is equal to the Burgers vector of the dislocation that generated the slip facet. The vertices of each slip facet are specified in a certain, consistent order, which determines the positive side of the facet. The sign of the facet's local slip vector is always consistent with this orientation sense.
Furthermore, each slip facet has a crystallographic normal vector that is written to the output file. 

The vertices of all slip facets are stored back to back in one contiguous NetCDF array (``slip_facet_vertices``).

Note that slip facets are constructed in the second configuration of the crystal, i.e. *after* crystal slip took place.

##### Additional NetCDF dimensions:

| Dimension name      | Value/Description    |
|---------------------|--------------------------------|
| ``slip_facets`` | Number of slip facets |
| ``slip_facet_vertices`` | Length of vertex list |

##### Additional NetCDF variables:

| Variable name      | Dimensions | Data type | Description    |
|--------------------|------------|-----------|----------------|
| slipped_edges      | ``slip_facets`` x ``pair`` | NC_TAGINT_TYPE | List of slipped Delaunay edges; pairs of atom IDs |
| slip_vectors       | ``slip_facets`` x ``spatial`` | NC_FLOAT | Crystallographic slip vectors of slip facets (given in lattice space) |
| slip_facet_normals | ``slip_facets`` x ``spatial`` | NC_FLOAT | Crystallographic normal vectors of slip facets (given in lattice space) |
| slip_facet_edge_counts | ``slip_facets``  | NC_INT | Number of vertices/edges of each slip facet |
| slip_facet_vertices | ``slip_facet_vertices``  | NC_TAGINT_TYPE | List of indices into the nodes list |

## Author and contact

* [Alexander Stukowski](http://www.mawi.tu-darmstadt.de/mm/mm_mm_sw/gruppe_mm_sw_1/mitarbeiterdetails_mm_2307.en.jsp),
  Technische Universität Darmstadt, Germany
