#!/bin/bash

lammps=$HOME/progs/lammps/src/lmp_mpi

file_list="initial.dump deformed.dump"
initial_file="initial.dump"

#file_list="initial_vacancies.dump deformed_vacancies.dump"
#initial_file="initial_vacancies.dump"

initial_timestep=0

#mpiexec-openmpi-mp -np 2 
${lammps} -in slip_analysis.in -log slip_analysis.log -var dump_files "${file_list}" -var initial_file "${initial_file}" -var initial_timestep ${initial_timestep}
