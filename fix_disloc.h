/* ----------------------------------------------------------------------
 * LAMMPS fix that extracts dislocations from a crystal simulation.
 *
 * Author: Alexander Stukowski (stukowski@mm.tu-darmstadt.de)
------------------------------------------------------------------------- */

#ifdef FIX_CLASS

FixStyle(disloc, DislocationIdentificationFix)

#else

#ifndef __FIX_DISLOC_LAMMPS_H
#define __FIX_DISLOC_LAMMPS_H

#include "fix.h"
#include "memory.h"
#include "fix_store.h"
#include "fix_ave_atom.h"
#include "compute.h"

#include "fix_disloc_delaunay.h"

using namespace LAMMPS_NS;

class DislocationIdentificationFix : public Fix
{
public:

    /// 3d vector data structure used throughout this code.
    class Vector3 
    {
    public:
        Vector3() {}
        Vector3(double x, double y, double z) { _data[0] = x; _data[1]= y; _data[2] = z; }
        double x() const { return _data[0]; }
        double y() const { return _data[1]; }
        double z() const { return _data[2]; }
        double operator[](size_t index) const { return _data[index]; }
        double& operator[](size_t index) { return _data[index]; }
        double squared_length() const { return x()*x() + y()*y() + z()*z(); }
        double length() const { return sqrt(squared_length()); }
        Vector3 operator-(const Vector3& v) const { return Vector3( x() - v.x(), y() - v.y(), z() - v.z() ); }
        Vector3 operator+(const Vector3& v) const { return Vector3( x() + v.x(), y() + v.y(), z() + v.z() ); }
        double dot(const Vector3& v) const { return x() * v.x() + y() * v.y() + z() * v.z(); }
        Vector3& operator+=(const Vector3& v) { _data[0] += v._data[0]; _data[1] += v._data[1]; _data[2] += v._data[2]; return *this; }
        Vector3& operator-=(const Vector3& v) { _data[0] -= v._data[0]; _data[1] -= v._data[1]; _data[2] -= v._data[2]; return *this; }
        Vector3& operator/=(double d) { _data[0] /= d; _data[1] /= d; _data[2] /= d; return *this; }
        friend Vector3 operator*(double d, const Vector3& v) { return Vector3(v[0]*d, v[1]*d, v[2]*d); }
        Vector3 operator-() const { return Vector3(-x(), -y(), -z()); }
        Vector3 cross(const Vector3& b) const { 
            return Vector3(y() * b.z() - z() * b.y(), z() * b.x() - x() * b.z(), x() * b.y() - y() * b.x());
        }
    private:
        double _data[3];
    };

    /// There is only a finite set of lattice vectors the code ever has to deal with.
    /// Thus we can use indices into a precomputed lattice vector table instead of the actually vector components 
    /// to store them. This reduces the required memory to store a large number of lattice vectors
    /// and vector addition/substraction can be speed up using lookup tables.
    /// The following data type is used for storing indices into the hard-coded lattice VectorList.
    typedef int LatticeVectorType;

    /// Helper class that stores a list of 3d vectors.
    /// It is used as lookup table to convert vector indices (LatticeVectorType) into xyz components and vice versa.
    class VectorList : public std::vector<Vector3>
    {
    public:

        /// Returns the index of the vector from the list that is closest to the given vector.
        /// Returns -1 if no vector in the list is close enough.
        LatticeVectorType closestPrimaryVector(const Vector3& v, size_t primaryListSize) const {
            LatticeVectorType result = 0;
            double maxDeviation = v.squared_length();
            for(size_t i = 1; i < primaryListSize; i++) {
                double d = (v - (*this)[i]).squared_length();
                if(d < maxDeviation) {
                    maxDeviation = d;
                    result = i;
                }
            }
            return result;
        }

        /// Returns the index of the vector from a sublist of this list that is closest to the given vector.
        /// Returns -1 if no vector in the sublist is close enough.
        LatticeVectorType closestVectorFromSubset(const Vector3& v, const LatticeVectorType* sublist, size_t sublist_size, double maxDeviation = std::numeric_limits<double>::max()) const {
            LatticeVectorType result = -1;
            for(size_t i = 0; i < sublist_size; i++) {
                double d = (v - (*this)[sublist[i]]).squared_length();
                if(d < maxDeviation) {
                    maxDeviation = d;
                    result = sublist[i];
                }
            }
            return result;
        }

        /// Finds the index of a certain vector in this list.
        /// Returns -1 if no such vector exists in the list.
        LatticeVectorType find(const Vector3& v, double maxDeviation = 1e-6) const {
            for(size_t i = 0; i < size(); i++) {
#if 1
                if((v - (*this)[i]).squared_length() <= maxDeviation)
#else
                const Vector3& vi = (*this)[i];
                if(std::fabs(v[0] - vi[0]) <= maxDeviation && std::fabs(v[1] - vi[1]) <= maxDeviation && std::fabs(v[2] - vi[2]) <= maxDeviation)
#endif
                    return i;
            }
            return -1;
        }
    };

    struct Edge; // Forward declaration, see below.

    // Data structure for tetrahedral cells of the tessellation.
    struct CellEdgeList 
    {
        Edge* edges[6]; // Each cell has six edges.
    };

    // A facet is defined by the tetrahedral cell index and the face index (0-3).
    typedef std::pair<GEO::index_t, int> Facet;

    // Data structure representing a (directed) edge of the Delaunay tessellation.
    struct Edge 
    {
        Edge(GEO::index_t v2, bool isOddEdge) : latticeVector(-1), deformedLatticeVector(-1), vertex2(v2), flags(isOddEdge ? IS_ODD_EDGE : 0) {}
        Edge* nextLeavingEdge;    // The next edge in the vertex' linked list of edges.
        GEO::index_t vertex2;     // The vertex this edge points to.
        Facet incidentFacet;
        LatticeVectorType latticeVector;          // The index of the ideal lattice vector assigned to this edge.
        LatticeVectorType initialLatticeVector;   // A copy of the value above, kept for reference.
        LatticeVectorType deformedLatticeVector;  // The index of the ideal lattice vector in the deformed configuration.
        signed short flags;                       // Stores the bit flags associated with this edge. 

        enum {
            IS_FLAGGED_EDGE = (1<<0),
            IS_LOCAL_EDGE = (1<<1),
            IS_ODD_EDGE = (1<<2),
            IS_INITIALLY_FIXED_EDGE = (1<<3),
            IS_FIXED_EDGE = (1<<4)
        };

        /// Returns the opposite edge, pointing to the vertex this edge originates from.
        Edge* oppositeEdge() const {
            return isOddEdge() ? (const_cast<Edge*>(this)-1) : (const_cast<Edge*>(this)+1);
        }

        /// Returns true if this edge's index is odd.
        bool isOddEdge() const { return (flags & IS_ODD_EDGE) != 0; }

        void flagEdge() { oppositeEdge()->setFlagged(); this->setFlagged(); }

        bool isFlagged() const { return (flags & IS_FLAGGED_EDGE) != 0; }
        void setFlagged() { flags |= IS_FLAGGED_EDGE; }
        void unsetFlagged() { flags &= ~IS_FLAGGED_EDGE; }

        // Indicates whether this edge belongs to the local processor.
        bool isLocal() const { return (flags & IS_LOCAL_EDGE) != 0; }
        void setIsLocal() { flags |= IS_LOCAL_EDGE; }

        // Indicates whether the lattice vector assigned to this edge has been fixed from the beginning.
        bool isInitiallyFixed() const { return (flags & IS_INITIALLY_FIXED_EDGE) != 0; }
        void setIsInitiallyFixed() { flags |= IS_INITIALLY_FIXED_EDGE; }
        void unsetIsInitiallyFixed() { flags &= ~IS_INITIALLY_FIXED_EDGE; }

        // Indicates whether the lattice vector assigned to this edge has been fixed.
        bool isFixed() const { return (flags & IS_FIXED_EDGE) != 0; }
        void setIsFixed() { flags |= IS_FIXED_EDGE; }
        void unsetIsFixed() { flags &= ~IS_FIXED_EDGE; }

        bool isFixedAny() const { return (flags & (IS_FIXED_EDGE|IS_INITIALLY_FIXED_EDGE)) != 0; }
    };

    // Simple memory pool for allocating 'Edge' structures.
    template<size_t PageSize = 16384>
    class EdgeMemoryPool : protected Pointers
    {
    public:
        class iterator {
        public:
            iterator(Edge* const* page, size_t index) : _page(page), _index(index) {}
            iterator& operator++() {
                if(++_index == PageSize) {
                    _index = 0;
                    ++_page;
                }
                return *this;
            }
            Edge* operator->() const {
                return &(*_page)[_index];
            }
            Edge& operator*() const {
                return (*_page)[_index];
            }
            bool operator!=(const iterator& other) const { return _page != other._page || _index != other._index; }
        private:
            Edge* const* _page;
            size_t _index;
        };
    public:
        EdgeMemoryPool(LAMMPS* ptr) : Pointers(ptr), _numAllocated(0) {}
        ~EdgeMemoryPool() {
            // Release memory pages.
            for(std::vector<Edge*>::iterator p = _pages.begin(); p != _pages.end(); ++p)
                memory->sfree(*p);
        }
        Edge* construct(GEO::index_t v2, bool isOddEdge) {
            if(_numAllocated == _pages.size() * PageSize) {
                // Allocate a new memory page.
                _pages.push_back(static_cast<Edge*>(memory->smalloc(PageSize * sizeof(Edge), "DislocationIdentificationFix::EdgeMemoryPool")));
            }
            Edge* edge = &_pages[_numAllocated / PageSize][_numAllocated % PageSize];
            // Call placement new operator to initialize Edge structure.
            new (edge)Edge(v2, isOddEdge);
            _numAllocated++;
            return edge;
        }
        void clear() {
            // Throw away objects, but hold on to allocated memory.
            _numAllocated = 0;
        }
        iterator begin() const { return iterator(_pages.data(), 0); }
        iterator end() const { return iterator(_pages.data() + (_numAllocated / PageSize), _numAllocated % PageSize); }
        Edge& at(size_t index) const {
            return _pages[index / PageSize][index % PageSize];
        }
        size_t memoryUsage() const { 
            return sizeof(Edge) * _pages.size() * PageSize;
        }
    private:
        std::vector<Edge*> _pages;
        size_t _numAllocated;
        std::allocator<Edge> _alloc;
    };

    /// Iterator class that visits all Delaunay facets that are adjacent to a given Delaunay edge.
    /// Instances of this class are returned by the incident_facets() method.
    class FacetCirculator 
    {
    public:
        FacetCirculator(const DislocationIdentificationFix& tess, GEO::index_t s, GEO::index_t t, GEO::index_t start, int f) :
            _tess(tess), _s(s), _t(t) {
            int i = tess.tessellation->index(start, _s);
            int j = tess.tessellation->index(start, _t);
            if(f == next_around_edge(i,j))
                _pos = start;
            else
                _pos = tess.tessellation->cell_adjacent(start, f); // other cell with same facet
        }
        FacetCirculator& operator=(const FacetCirculator& other) {
            _pos = other._pos;
            return *this;
        }
        FacetCirculator& operator--() {
            _pos = _tess.tessellation->cell_adjacent(_pos, next_around_edge(_tess.tessellation->index(_pos, _t), _tess.tessellation->index(_pos, _s)));
            return *this;
        }
        FacetCirculator operator--(int) {
            FacetCirculator tmp(*this);
            --(*this);
            return tmp;
        }
        FacetCirculator & operator++() {
            _pos = _tess.tessellation->cell_adjacent(_pos, next_around_edge(_tess.tessellation->index(_pos, _s), _tess.tessellation->index(_pos, _t)));
            return *this;
        }
        FacetCirculator operator++(int) {
            FacetCirculator tmp(*this);
            ++(*this);
            return tmp;
        }
        Facet operator*() const {
            return Facet(_pos, next_around_edge(_tess.tessellation->index(_pos, _s), _tess.tessellation->index(_pos, _t)));
        }
        Facet operator->() const {
            return Facet(_pos, next_around_edge(_tess.tessellation->index(_pos, _s), _tess.tessellation->index(_pos, _t)));
        }
        GEO::index_t cell() const { return _pos; }
        int facet() const { return next_around_edge(_tess.tessellation->index(_pos, _s), _tess.tessellation->index(_pos, _t)); }
        bool operator==(const FacetCirculator& ccir) const
        {
            return _pos == ccir._pos && _s == ccir._s && _t == ccir._t;
        }
        bool operator!=(const FacetCirculator& ccir) const
        {
            return ! (*this == ccir);
        }

        static int next_around_edge(int i, int j) {
            static const int tab_next_around_edge[4][4] = {
                  {5, 2, 3, 1},
                  {3, 5, 0, 2},
                  {1, 3, 5, 0},
                  {2, 0, 1, 5} };
            return tab_next_around_edge[i][j];
        }

    private:
        const DislocationIdentificationFix& _tess;
        GEO::index_t _s;
        GEO::index_t _t;
        GEO::index_t _pos;
    };

    /// A bit-flag array that stores which pairs of neighbors are bonded
    /// and which are not (using bit-wise storage).
    struct NeighborBondArray
    {
        /// Maximum number of neighbors in coordination structures recognized by the CNA algorithm.
        enum { CNA_MAX_PATTERN_NEIGHBORS = 14 };

        /// Two-dimensional bit array that stores the bonds between neighbors.
        unsigned int neighborArray[CNA_MAX_PATTERN_NEIGHBORS];

        /// Default constructor.
        NeighborBondArray() { memset(neighborArray, 0, sizeof(neighborArray)); }

        /// Returns whether two nearest neighbors have a bond between them.
        bool neighborBond(int neighborIndex1, int neighborIndex2) const {
            return (neighborArray[neighborIndex1] & (1<<neighborIndex2));
        }

        /// Sets whether two nearest neighbors have a bond between them.
        void setNeighborBond(int neighborIndex1, int neighborIndex2) {
            neighborArray[neighborIndex1] |= (1<<neighborIndex2);
            neighborArray[neighborIndex2] |= (1<<neighborIndex1);
        }
    };    

public:

    /// Constructor.
    DislocationIdentificationFix(class LAMMPS*, int, char **);

    /// Destructor.
    virtual ~DislocationIdentificationFix();

    /// Called by the system after the fix has been created.
    virtual void post_constructor();

    /// The return value of this method specifies at which points the fix is invoked during the simulation.
    virtual int setmask();

    /// This gets called by the system before the simulation starts.
    virtual void init();

    /// This is called by LAMMPS at the beginning of a run.
    virtual void setup(int);

    /// Called at the end of MD time integration steps.
    virtual void end_of_step();

    /// Reports the memory usage of this fix to LAMMPS.
    virtual double memory_usage();

    /// Initialize one atom's storage values, called when atom is created.
    virtual void set_arrays(int);

    /// Neighbor list initialization.
    virtual void init_list(int id, NeighList *ptr) {
        list = ptr;
    }

    /// This is for MPI communication with neighbor nodes.
    virtual int pack_forward_comm(int n, int *list, double *buf, int pbc_flag, int *pbc);
    virtual void unpack_forward_comm(int n, int first, double *buf);

    /// Is called by the system to request the global vector computed by the fix.
    virtual double compute_vector(int n);

private:

    /// Sends a formatted string to the log file and stdout.
    void printLog(const char* format, ...);

    /// Precomputes the ideal lattice vectors.
    void prepareLatticeVectors();

    /// Precomputes the rotated lattice vectors.
    void prepareRotatedLatticeVectors(double orientation[3][3]);

    /// Computes the atomic displacement vectors.
    void computeDisplacements();

    /// Calls the GEOGRAM library to compute the Delaunay tessellation.
    /// The atomic coordinates serve as input vertices for the tessellation.
    void generateTessellation();

    /// Extracts the edges of the tessellation.
    void createTessellationEdges();

    /// Assigns ideal lattice vectors to edges of the tessellation.
    void assignLatticeVectors(bool displacedConfig);

    /// Identify local atomic structure.
    void identifyAtomicStructure(bool displacedConfig = false);

    /// Makes sure lattice vectors assigned to edges are consistent across processors.
    void synchronizeEdgeVectors(bool onlyChangedEdges);

    /// Optimizes the lattice vectors assigned to tessellation edges.
    void optimizeAssignedLatticeVectors();

    /// Modifies the elastic field such that small dislocation loops are eliminated.
    void eliminateSmallLoops();

    /// Modifies the elastic field such that unnecessary dislocations are eliminated.
    void eliminateLineLength();

    /// Eliminates slip surface artifacts.
    void eliminateSlipSurfaceArtifacts();

    /// Modifies the lattice vector assigned to a Delaunay edge.
    int improveEdgeLatticeVectorRecursive(std::pair<Edge*,LatticeVectorType>* results, int depth);

    /// Returns the edge connecting two vertices of the tessellation,
    /// or NULL if no such edge exists.
    Edge* findEdge(GEO::index_t v1, GEO::index_t v2) const {
        for(Edge* e = vertexEdges[v1]; e != NULL; e = e->nextLeavingEdge)
            if(e->vertex2 == v2) return e;
        return NULL;
    }

    /// Computes the sum of two lattice vectors.
    /// Returns -1 if the summed vector is not indexed because it is too long.
    LatticeVectorType vectorSum(LatticeVectorType v1, LatticeVectorType v2) const {
        return latticeVectorSums[v1][v2];
    }

    /// Returns whether the given tessellation cell connects four physical vertices.
    /// Returns false if one of the four vertices is the infinite vertex.
    bool isFiniteCell(GEO::index_t cell) const {
        return tessellation->cell_is_finite(cell);
    }

    Facet mirrorFacet(GEO::index_t cell, int face) const {
        GEO::signed_index_t adjacentCell = tessellation->cell_adjacent(cell, face);
        return Facet(adjacentCell, tessellation->adjacent_index(adjacentCell, cell));
    }

    FacetCirculator incident_facets(GEO::index_t cell, int i, int j, GEO::index_t start, int f) const {
        return FacetCirculator(*this, tessellation->cell_vertex(cell, i), tessellation->cell_vertex(cell, j), start, f);
    }

    FacetCirculator incident_facets(GEO::index_t s, GEO::index_t t, GEO::index_t start, int f) const {
        return FacetCirculator(*this, s, t, start, f);
    }

    /// Returns the cell vertex for the given triangle vertex of the given cell facet.
    static inline int cellFacetVertexIndex(int cellFacetIndex, int facetVertexIndex) {
        static const int tab_vertex_triple_index[4][3] = {
         {1, 3, 2},
         {0, 2, 3},
         {0, 3, 1},
         {0, 1, 2}
        };
        return tab_vertex_triple_index[cellFacetIndex][facetVertexIndex];
    }

    /// Returns the edge connecting the two vertices i and j of a tetrahedral cell.
    inline Edge* getCellEdge(GEO::index_t cell, int i, int j) {
        static const int edge_lookup[4][4] = {
                { 6, 0, 1, 2 },
                { -1, 6, 3, 4 },
                { -2, -4, 6, 5 },
                { -3, -5, -6, 6 }
        };
        if(edge_lookup[i][j] >= 0)
            return cellEdges[cell].edges[edge_lookup[i][j]];
        else
            return cellEdges[cell].edges[-edge_lookup[i][j]-1]->oppositeEdge();
    }

    LatticeVectorType computeAlternativeEdgeLatticeVector(GEO::index_t cell, int i, int j, Edge*& edge1, Edge*& edge2) {
        static const int edge_pairs[4][4][2] = {
                {{7,7}, {3,-5}, {1,4}, {2,6}},
                {{4,-2}, {7,7}, {5,-6}, {-1,3}},
                {{6,-3}, {-2,1}, {7,7}, {-4,5}},
                {{-5,-1}, {-6,-4}, {-3,2}, {7,7}}
        };

        if(edge_pairs[i][j][0] > 0)
            edge1 = cellEdges[cell].edges[edge_pairs[i][j][0]-1];
        else
            edge1 = cellEdges[cell].edges[-edge_pairs[i][j][0]-1]->oppositeEdge();
        if(edge_pairs[i][j][1] > 0)
            edge2 = cellEdges[cell].edges[edge_pairs[i][j][1]-1];
        else
            edge2 = cellEdges[cell].edges[-edge_pairs[i][j][1]-1]->oppositeEdge();

        if(edge1->latticeVector != -1 && edge2->latticeVector != -1)
            return vectorSum(edge1->latticeVector, edge2->latticeVector);
        else
            return -1;
    }

    /// Computes the Burgers vector associated with a Delaunay facet.
    LatticeVectorType computeFacetBurgersVector(Facet facet) const;

    /// Computes how much plastic slip ocurred on each slip system
    /// and the total plastic displacement gradient.
    void computeSweptAreaPerSlipSystem();

    /// Generates the output file.
    void writeOutputFile(const std::string& filename);

    /// Collects the dislocation and slip facet data to be written to the output file.
    void gatherOutputData(std::vector<GEO::index_t>& outputCells, std::vector<int>& outputCellIndices, 
        std::vector<tagint>& dislocationSegments, std::vector<float>& segmentBurgersVectors, 
        std::vector<float>& cellCenters, std::vector<tagint>& cellIds,
        std::vector<tagint>& slippedEdges, std::vector<float>& slipVectors, std::vector<float>& slipFacetNormals, 
        std::vector<tagint>& slipSurfaceVertices, std::vector<int>& slipSurfaceEdgeCounts);

    /// Writes a LAMMPS data file with the vertices and edges of the tessellation.
    void dumpTessellationToDataFile(const std::string& filename);

    /// Writes a VTK file containing all triangle facets intersected by a dislocation.
    void dumpDislocatedFacets(const std::string& filename);

protected:

    /// Base name of the output files.
    std::string outputBasename;

    /// The name of the input lattice structure, e.g. "bcc".
    std::string latticeStructure;

    /// Matrix that rotates vectors from the global simulation coordinate system to the local lattice coordinate system.
    double lattice_rotation[3][3];

    /// Matrix that rotates lattice vectors to the global simulation coordinate system.
    double inv_lattice_rotation[3][3];

    /// The name of the LAMMPS compute that determines the current crystal orientation.
    std::string crystalorientComputeName;
    
    /// The compute that determines the current lattice orientation.
    Compute* crystalorient_compute;

    // A user-defined fix that computes time-averaged atomic positions.
    FixAveAtom* averageFix;
    double **averagePositions;

    /// The lattice scaling parameter.
    double latticeParameter;

    /// The list of ideal lattice vectors.
    VectorList latticeVectors;

    size_t primaryLatticeVectorCount;

    /// List of lattice vectors, which have been scaled by the lattice constant.
    VectorList scaledLatticeVectors;

    /// The lookup table for reverse vectors.
    std::vector<LatticeVectorType> reverseLatticeVectors;

    /// Lookup table for vector sums.
    /// For two lattice vectors a and b it gives (a+b).
    std::vector< std::vector<LatticeVectorType> > latticeVectorSums;

    /// Boolean array that indicates for every lattice vector whether it is a full lattice vector.
    std::vector<bool> isFullLatticeVector;

    /// This array stores for every fractional lattice vector the corresponding stacking fault displacement vector,
    /// or zero for full lattice vectors.
    std::vector<LatticeVectorType> stackingFaultVectors;    

    /// The CNA bond signatures of the reference structure templates. 
    std::vector< std::vector<int> > cnaSignaturesReference;

    /// The connectivity graph for the CNA reference structures.
    std::vector<NeighborBondArray> neighborArrayReference;

    /// Used for efficient allocation of Edge data structures.
    EdgeMemoryPool<> edgePool;

    /// Stores the heads of the linked lists of edges leaving each vertex.
    std::vector<Edge*> vertexEdges;

    /// Stores per-cell auxiliary data.
    std::vector<CellEdgeList> cellEdges;

    /// The internal Delaunay generator object.
    GEO::Delaunay_var tessellation;

    /// LAMMPS neighbor list.
    class NeighList *list;

    /// Stores per-atom structure type.
    std::vector<int> structureTypes;

    /// Number of edge records to transmit per atom and per inter-processor communication pass.
    enum { EDGE_RECORDS_PER_COMM_PASS = 4 };

    /// Data structure used for transmitting edge lattice vectors between processors.
    struct EdgeCommRecord {
        tagint tagVertex2;
        LatticeVectorType lv;
    };

    /// Temporary buffer used for synchronizing edge lattice vectors.
    std::vector<EdgeCommRecord> edgeTransmissionBuffer;

    /// The structural templates.
    std::vector< std::vector<LatticeVectorType> > templateStructures;
    
    /// Number of neighbors in the current crystal lattice structure template.
    int numLatticeStructureNeighbors;

    /// The ID of the fix that stores the previous atomic positions.
    char* id_fix;

    /// Stores the atomic displacement vectors.
    Vector3* displacements;

    /// The allocated array size.
    int nmaxDisplacements;

    /// The fix that stores the previous atomic positions.
    FixStore* lastpos_fix;

    /// Determines the information to be transfered between procs.
    int commStage;

    /// Controls the extraction of dislocation slip surfaces.
    bool performSlipAnalysis;
    
    /// Controls whether slip surfaces are extracted on an incremental basis.
    bool incrementalSlipAnalysis;

    /// Controls whether atomic structure identification is used as an additional step to improve lattice vector assignment.
    bool useAtomicStructureIdentification;

    /// Activates the processing step that is supposed to clean up the assigned lattice vectors.
    bool optimizeLatticeVectorsEnabled;

    /// Activates the processing step that is supposed to eliminates small dislocation loop artifacts.
    bool eliminateSmallLoopsEnabled;

    bool eliminateLineLengthEnabled;

    /// Controls the extraction of stacking fault planes.
    bool extractStackingFaults;

    /// Enables debugging print statements.
    bool debugOutput;

    /// Build tessellation once in the beginning, never update it.
    bool useStaticTessellation;

    /// Flag that forces an analysis step at the beginning of an MD run.
    bool performAnalysisAtBeginningOfRun;

    /// Shape matrix of initial simulation cell in Voigt notation.
    double reference_h[6];

    /// Inverse shape matrix of initial simulation cell in Voigt notation.
    double reference_h_inv[6];

    /// The corner coordinates of the initial simulation cell.
    double reference_boxlo[3];

    /// The global plastic displacement gradient computed from the incremental slip analysis.
    double plastic_displ_gradient[3][3];

    /// The list of slip systems defined by the user.
    std::vector< std::pair<Vector3, Vector3> > slipSystems;

    /// The projected area of plastic slip on the different slip systems. 
    std::vector<double> slipSystemSweptAreas;

    /// Indicates whether the Burgers vectors of the dislocation lines generated by the algorithm should be flipped
    /// from RH/FS convention (the default) to LH/FS convention.
    bool reverseDislocationSense;

    /// Controls by how much output data should be accumulated by MPI tasks before it is dumped to disk.
    /// This is used to speed up I/O performance for large number of processors.
    int aggregateIOLevel;

    /// List of Burgers circuits on a tetrahedron, each defined by a sequence of three edges.
    static const int burgersCircuitEdges[4][3];
};

#endif // __FIX_DISLOC_LAMMPS_H

#endif // FIX_CLASS
