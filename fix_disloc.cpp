/* ----------------------------------------------------------------------
 * LAMMPS fix that identifies dislocation lines in a crystal model.
 *
 * Author: Alexander Stukowski (stukowski@mm.tu-darmstadt.de)
------------------------------------------------------------------------- */

#include "fix_disloc.h"
#include "force.h"
#include "comm.h"
#include "modify.h"
#include "domain.h"
#include "error.h"
#include "atom.h"
#include "neighbor.h"
#include "neigh_list.h"
#include "neigh_request.h"
#include "error.h"
#include "update.h"
#include "group.h"
#include "universe.h"
#include "random_park.h"

#include <fstream>
#include <sstream>
#include <cstdio>
#include <algorithm>
#include <numeric>
#include <set>
#include <cstdarg>
#include <cassert>
#include <chrono>

#include <sys/stat.h>
#include <errno.h>

#define INVOKED_VECTOR 2

// Controls whether the output file is written using parallel I/O.
// If disabled, each MPI task will send its data to the master task, which is responsible for writing to the output file.
#define FIX_DISLOC_USE_PARALLEL_IO        0

// Make sure NetCDF is available:
#ifndef LMP_HAS_NETCDF
#error "LAMMPS has not been configured with NetCDF support. Make sure 'nc-config' is in your PATH, or specify the location of NetCDF library files. See lib/netcdf/README for more information."
#endif

// Select the NetCDF data type to use when storing values of LAMMPS data type 'tagint':
#if defined(LAMMPS_SMALLBIG)
    #define NC_TAGINT_TYPE NC_INT
    #define nc_put_vara_tagint nc_put_vara_int
#elif defined(LAMMPS_BIGBIG)
    #define NC_TAGINT_TYPE NC_INT64
    #define nc_put_vara_tagint nc_put_vara_longlong
#elif defined(LAMMPS_SMALLSMALL)
    #define NC_TAGINT_TYPE NC_INT
    #define nc_put_vara_tagint nc_put_vara_int
#endif

#if FIX_DISLOC_USE_PARALLEL_IO
    #include <netcdf_par.h>
#endif
#include <netcdf.h>

/// This is to identify the code version in use in the log and output data files.
#define FIX_DISLOC_CODE_VERSION         "1.14"
/// This is to identify the output file format version written by the fix.
#define FIX_DISLOC_FORMAT_VERSION       "1.1"

// List of Burgers circuits on a tetrahedron, each defined by a sequence of three edges.
const int DislocationIdentificationFix::burgersCircuitEdges[4][3] = {{3,5,4},{1,5,2},{0,4,2},{0,3,1}};

/******************************************************************************
* Constructor.
******************************************************************************/
DislocationIdentificationFix::DislocationIdentificationFix(LAMMPS *lmp, int narg, char **arg) :
    Fix(lmp, narg, arg),
    edgePool(lmp), displacements(NULL), nmaxDisplacements(0), lastpos_fix(NULL), id_fix(NULL),
    crystalorient_compute(NULL),
    averageFix(NULL), averagePositions(NULL)
{
    // Let LAMMPS know the number of data values per atom to transfer via MPI interprocessor communication.
    this->comm_forward = std::max(3, EDGE_RECORDS_PER_COMM_PASS * 2);

    // Default settings:
    bool enableRefinementProcedures = true;
    performSlipAnalysis = false;
    incrementalSlipAnalysis = false;
    useAtomicStructureIdentification = enableRefinementProcedures;
    extractStackingFaults = false;
    debugOutput = false;
    performAnalysisAtBeginningOfRun = true;
    eliminateSmallLoopsEnabled = enableRefinementProcedures;
    optimizeLatticeVectorsEnabled = enableRefinementProcedures;
    useStaticTessellation = false;
    eliminateLineLengthEnabled = enableRefinementProcedures;
    memset(plastic_displ_gradient, 0, sizeof(plastic_displ_gradient));
    reverseDislocationSense = false;
    aggregateIOLevel = 0;

    // Parse fix command parameters.
    int iarg = 3;
    if(narg < 7) error->all(FLERR, "Not enough parameters specified for fix disloc.");

    // Timestep interval for extracting dislocations.
    this->nevery = utils::inumeric(FLERR, arg[iarg++], false, lmp);
    if(this->nevery < 1) error->all(FLERR,"Invalid timestep parameter for fix disloc.");

    // Select lattice type and initialize ideal lattice vectors.
    latticeStructure = arg[iarg++];
    if(latticeStructure == "bcc") {
        // 1/2<111> nearest neighbor vectors in standard Bravais cell:
        latticeVectors.push_back(Vector3(0.5,0.5,0.5));
        latticeVectors.push_back(Vector3(-0.5,0.5,0.5));
        latticeVectors.push_back(Vector3(0.5,-0.5,0.5));
        latticeVectors.push_back(Vector3(-0.5,-0.5,0.5));
        latticeVectors.push_back(Vector3(0.5,0.5,-0.5));
        latticeVectors.push_back(Vector3(-0.5,0.5,-0.5));
        latticeVectors.push_back(Vector3(0.5,-0.5,-0.5));
        latticeVectors.push_back(Vector3(-0.5,-0.5,-0.5));
        // <100> second nearest neighbor vectors in standard Bravais cell:
        latticeVectors.push_back(Vector3(1.0,0.0,0.0));
        latticeVectors.push_back(Vector3(-1.0,0.0,0.0));
        latticeVectors.push_back(Vector3(0.0,1.0,0.0));
        latticeVectors.push_back(Vector3(0.0,-1.0,0.0));
        latticeVectors.push_back(Vector3(0.0,0.0,1.0));
        latticeVectors.push_back(Vector3(0.0,0.0,-1.0));

        isFullLatticeVector.resize(latticeVectors.size(), true);
        numLatticeStructureNeighbors = 14;
    }
    else if(latticeStructure == "fcc") {
        // 1/2<110> nearest neighbor vectors in standard Bravais cell:
        latticeVectors.push_back(Vector3( 0.5,  0.5,  0.0));
        latticeVectors.push_back(Vector3( 0.0,  0.5,  0.5));
        latticeVectors.push_back(Vector3( 0.5,  0.0,  0.5));
        latticeVectors.push_back(Vector3(-0.5, -0.5,  0.0));
        latticeVectors.push_back(Vector3( 0.0, -0.5, -0.5));
        latticeVectors.push_back(Vector3(-0.5,  0.0, -0.5));
        latticeVectors.push_back(Vector3(-0.5,  0.5,  0.0));
        latticeVectors.push_back(Vector3( 0.0, -0.5,  0.5));
        latticeVectors.push_back(Vector3(-0.5,  0.0,  0.5));
        latticeVectors.push_back(Vector3( 0.5, -0.5,  0.0));
        latticeVectors.push_back(Vector3( 0.0,  0.5, -0.5));
        latticeVectors.push_back(Vector3( 0.5,  0.0, -0.5));
        isFullLatticeVector.resize(latticeVectors.size(), true);
        // 1/6<112> partial vectors:
        latticeVectors.push_back(Vector3( 0.3333333333333,  0.1666666666666,  0.1666666666666));
        latticeVectors.push_back(Vector3( 0.1666666666666,  0.3333333333333,  0.1666666666666));
        latticeVectors.push_back(Vector3( 0.1666666666666,  0.1666666666666,  0.3333333333333));
        latticeVectors.push_back(Vector3(-0.3333333333333,  0.1666666666666,  0.1666666666666));
        latticeVectors.push_back(Vector3(-0.1666666666666,  0.3333333333333,  0.1666666666666));
        latticeVectors.push_back(Vector3(-0.1666666666666,  0.1666666666666,  0.3333333333333));
        latticeVectors.push_back(Vector3( 0.3333333333333, -0.1666666666666,  0.1666666666666));
        latticeVectors.push_back(Vector3( 0.1666666666666, -0.3333333333333,  0.1666666666666));
        latticeVectors.push_back(Vector3( 0.1666666666666, -0.1666666666666,  0.3333333333333));
        latticeVectors.push_back(Vector3( 0.3333333333333,  0.1666666666666, -0.1666666666666));
        latticeVectors.push_back(Vector3( 0.1666666666666,  0.3333333333333, -0.1666666666666));
        latticeVectors.push_back(Vector3( 0.1666666666666,  0.1666666666666, -0.3333333333333));
        latticeVectors.push_back(Vector3(-0.3333333333333, -0.1666666666666,  0.1666666666666));
        latticeVectors.push_back(Vector3(-0.1666666666666, -0.3333333333333,  0.1666666666666));
        latticeVectors.push_back(Vector3(-0.1666666666666, -0.1666666666666,  0.3333333333333));
        latticeVectors.push_back(Vector3( 0.3333333333333, -0.1666666666666, -0.1666666666666));
        latticeVectors.push_back(Vector3( 0.1666666666666, -0.3333333333333, -0.1666666666666));
        latticeVectors.push_back(Vector3( 0.1666666666666, -0.1666666666666, -0.3333333333333));
        latticeVectors.push_back(Vector3(-0.3333333333333,  0.1666666666666, -0.1666666666666));
        latticeVectors.push_back(Vector3(-0.1666666666666,  0.3333333333333, -0.1666666666666));
        latticeVectors.push_back(Vector3(-0.1666666666666,  0.1666666666666, -0.3333333333333));
        latticeVectors.push_back(Vector3(-0.3333333333333, -0.1666666666666, -0.1666666666666));
        latticeVectors.push_back(Vector3(-0.1666666666666, -0.3333333333333, -0.1666666666666));
        latticeVectors.push_back(Vector3(-0.1666666666666, -0.1666666666666, -0.3333333333333));
        isFullLatticeVector.resize(latticeVectors.size(), false);
        numLatticeStructureNeighbors = 12;
    }
    else if(latticeStructure == "fcc_perfect") {
        // 1/2<110> nearest neighbor vectors in standard Bravais cell:
        latticeVectors.push_back(Vector3( 0.5,  0.5,  0.0));
        latticeVectors.push_back(Vector3( 0.0,  0.5,  0.5));
        latticeVectors.push_back(Vector3( 0.5,  0.0,  0.5));
        latticeVectors.push_back(Vector3(-0.5, -0.5,  0.0));
        latticeVectors.push_back(Vector3( 0.0, -0.5, -0.5));
        latticeVectors.push_back(Vector3(-0.5,  0.0, -0.5));
        latticeVectors.push_back(Vector3(-0.5,  0.5,  0.0));
        latticeVectors.push_back(Vector3( 0.0, -0.5,  0.5));
        latticeVectors.push_back(Vector3(-0.5,  0.0,  0.5));
        latticeVectors.push_back(Vector3( 0.5, -0.5,  0.0));
        latticeVectors.push_back(Vector3( 0.0,  0.5, -0.5));
        latticeVectors.push_back(Vector3( 0.5,  0.0, -0.5));
        isFullLatticeVector.resize(latticeVectors.size(), true);
        numLatticeStructureNeighbors = 12;
    }
    else error->all(FLERR,"Invalid lattice type in fix disloc command.");

    // Lattice parameter.
    latticeParameter = utils::numeric(FLERR, arg[iarg++], false, lmp);
    if(latticeParameter <= 0.0) error->all(FLERR,"Invalid lattice parameter in fix disloc command.");

    // The lattice orientation within the global simulation coordinate system.
    // Assume identity matrix by default, i.e., Bravais cell is aligned with coordinate axes.
    double orientation[3][3] = {{1, 0, 0}, {0, 1, 0}, {0, 0, 1}};

    // Parse extra keyword parameters.
    while(iarg < narg) {
        if(strcmp(arg[iarg], "file") == 0) {
            // Output base name
            if(iarg + 1 > narg) error->all(FLERR,"Illegal 'file' keyword");
            outputBasename = arg[iarg+1];
            iarg += 2;
        }
        else if(strcmp(arg[iarg], "orient") == 0) {
            if(iarg + 1 > narg) error->all(FLERR,"Illegal 'orient' keyword");
            if(arg[iarg+1][0] == 'c' && arg[iarg+1][1] == '_') {
                crystalorientComputeName = arg[iarg+1] + 2;
                int icompute = modify->find_compute(crystalorientComputeName.c_str());
                if(icompute < 0)
                    error->all(FLERR,"Compute ID for fix disloc does not exist");
                iarg += 2;
            }
            else {
                if(iarg + 5 > narg) error->all(FLERR,"Illegal 'orient' keyword");
                int dim = -1;
                if(strcmp(arg[iarg+1],"x") == 0) dim = 0;
                else if(strcmp(arg[iarg+1],"y") == 0) dim = 1;
                else if(strcmp(arg[iarg+1],"z") == 0) dim = 2;
                else error->all(FLERR,"Invalid axis in 'orient' keyword");
                orientation[dim][0] = utils::numeric(FLERR, arg[iarg+2], false, lmp);
                orientation[dim][1] = utils::numeric(FLERR, arg[iarg+3], false, lmp);
                orientation[dim][2] = utils::numeric(FLERR, arg[iarg+4], false, lmp);
                iarg += 5;
            }
        }
        else if(strcmp(arg[iarg], "slip") == 0) {
            performSlipAnalysis = true;
            iarg += 1;
        }
        else if(strcmp(arg[iarg], "incremental") == 0) {
            incrementalSlipAnalysis = true;
            iarg += 1;
        }
        else if(strcmp(arg[iarg], "stackingfaults") == 0) {
            extractStackingFaults = true;
            iarg += 1;
        }
        else if(strcmp(arg[iarg], "debug") == 0) {
            debugOutput = true;
            iarg += 1;
        }
        else if(strcmp(arg[iarg], "optimize") == 0) {
            optimizeLatticeVectorsEnabled = true;
            iarg += 1;
        }
        else if(strcmp(arg[iarg], "loops") == 0) {
            eliminateSmallLoopsEnabled = true;
            iarg += 1;
        }
        else if(strcmp(arg[iarg], "struct") == 0) {
            useAtomicStructureIdentification = true;
            iarg += 1;
        }
        else if(strcmp(arg[iarg], "linelen") == 0) {
            eliminateLineLengthEnabled = true;
            iarg += 1;
        }
        else if(strcmp(arg[iarg], "statictess") == 0) {
            useStaticTessellation = true;
            iarg += 1;
        }
        else if(strcmp(arg[iarg], "slipsystem") == 0) {
            if(iarg + 6 > narg) error->all(FLERR,"Illegal 'slipsystem' keyword");
            Vector3 slipDirection, slipPlaneNormal;
            slipDirection[0] = utils::numeric(FLERR, arg[iarg+1], false, lmp);
            slipDirection[1] = utils::numeric(FLERR, arg[iarg+2], false, lmp);
            slipDirection[2] = utils::numeric(FLERR, arg[iarg+3], false, lmp);
            slipPlaneNormal[0] = utils::numeric(FLERR, arg[iarg+4], false, lmp);
            slipPlaneNormal[1] = utils::numeric(FLERR, arg[iarg+5], false, lmp);
            slipPlaneNormal[2] = utils::numeric(FLERR, arg[iarg+6], false, lmp);
            if(slipPlaneNormal.squared_length() < 1e-9)
                error->all(FLERR,"Slip plane normal following 'slipsystem' keyword must be non-zero");
            slipPlaneNormal /= slipPlaneNormal.length();
            if(fabs(slipDirection.dot(slipPlaneNormal)) > 1e-3)
                error->all(FLERR,"Slip plane normal and slip direction following 'slipsystem' keyword must be orthogonal");
            for(size_t i = 0; i < slipSystems.size(); i++) {
                if((slipSystems[i].first - slipDirection).squared_length() <= 1e-9)
                    error->all(FLERR,"Duplicate slip direction in slip system definition detected");
                if((slipSystems[i].first + slipDirection).squared_length() <= 1e-9) {
                    if((slipSystems[i].second + slipPlaneNormal).squared_length() > 1e-9) {
                        error->all(FLERR,"Inconsistent definition of opposite slip system detected");
                    }
                }
            }
            slipSystems.push_back(std::make_pair(slipDirection, slipPlaneNormal));
            iarg += 7;
        }
        else if(strcmp(arg[iarg], "convention") == 0) {
            if(iarg + 1 > narg) error->all(FLERR,"Illegal 'convention' keyword");
            if(strcmp(arg[iarg+1], "RH/FS") == 0) {
                reverseDislocationSense = false;
            }
            else if(strcmp(arg[iarg+1], "LH/FS") == 0) {
                reverseDislocationSense = true;
            }
            else {
                error->all(FLERR,"Invalid value specified for 'convention' setting");
            }
            iarg += 2;
        }
        else if(strcmp(arg[iarg], "aggregate_io") == 0) {
            if(iarg + 1 > narg) error->all(FLERR,"Illegal 'aggregate_io' keyword");
            aggregateIOLevel = utils::inumeric(FLERR, arg[iarg+1], false, lmp);
            iarg += 2;
        }
        else if(strcmp(arg[iarg], "ave") == 0) {
            if(iarg + 1 > narg) error->all(FLERR,"Illegal 'ave' keyword");
            int ifix = modify->find_fix(arg[iarg+1]);
            if(ifix < 0) error->all(FLERR,"No fix ave/atom with the given ID.");
            averageFix = dynamic_cast<FixAveAtom*>(modify->fix[ifix]);
            if(averageFix == NULL) error->all(FLERR,"Fix referenced by 'ave' keyword is not an ave/atom fix.");
            if(averageFix->peratom_flag == 0 || averageFix->peratom_flag == 0 || averageFix->size_peratom_cols != 3)
                error->all(FLERR,"Fix ave/atom referenced by fix disloc does not provide three values per atom.");
            if(this->igroup != averageFix->igroup)
                error->all(FLERR,"Fix ave/atom referenced by the fix disloc is not using the same atom group.");
            iarg += 2;
        }
        else error->all(FLERR,"Unknown optional parameter in fix disloc command.");
    }

    if(performSlipAnalysis) {
        // Need to store previous atomic positions when extracting slip surfaces.
        this->create_attribute = 1;

        // Output plastic displacement gradient tensor as a global vector.
        this->vector_flag = true;
        this->size_vector = 9;
        this->global_freq = this->nevery;
        this->extvector = 0;

        // Fix outputs dislocation fluxes on the defined slip systems.
        if(!slipSystems.empty()) {
            this->size_vector += slipSystems.size();
            slipSystemSweptAreas.resize(slipSystems.size(), 0.0);
        }
    }

    prepareLatticeVectors();
    prepareRotatedLatticeVectors(orientation);

    if(useAtomicStructureIdentification) {
        if(latticeStructure == "bcc") {
            // Add BCC template.
            std::vector<LatticeVectorType> bcc_lattice_vectors(14);
            for(int i = 0; i < 14; i++) bcc_lattice_vectors[i] = i+1;
            templateStructures.push_back(bcc_lattice_vectors);
        }
        else if(latticeStructure == "fcc") {
            // Add master FCC template.
            std::vector<LatticeVectorType> fcc_lattice_vectors(12);
            for(int i = 0; i < 12; i++) fcc_lattice_vectors[i] = i+1;
            templateStructures.push_back(fcc_lattice_vectors);

            // Add FCC templates for twinned structures.
            static const Vector3 twinning_planes[4] = {
                Vector3(1,1,1), Vector3(-1,1,1), Vector3(1,-1,1), Vector3(1,1,-1)
            };
            std::vector<LatticeVectorType> twinned_fcc_lattice_vectors(12);
            for(int i = 0; i < 4; i++) {
                for(int j = 0; j < 12; j++) {
                    const Vector3& vec = latticeVectors[fcc_lattice_vectors[j]];
                    Vector3 twinned_vec = vec - 2.0 * vec.dot(twinning_planes[i]) / twinning_planes[i].squared_length() * twinning_planes[i];
                    twinned_fcc_lattice_vectors[j] = latticeVectors.find(twinned_vec);
                }
                std::sort(twinned_fcc_lattice_vectors.begin(), twinned_fcc_lattice_vectors.end());
                templateStructures.push_back(twinned_fcc_lattice_vectors);
            }

            // Add HCP templates needed for stacking faults.
            static const Vector3 org_hcp_lattice_vectors[][12] = {
                    { Vector3(-3.0/6, -3.0/6, 0.0/6), Vector3(-3.0/6, 0.0/6, -3.0/6), Vector3(-3.0/6, 0.0/6, 3.0/6), Vector3(-3.0/6, 3.0/6, 0.0/6), Vector3(0.0/6, -3.0/6, -3.0/6), Vector3(0.0/6, -3.0/6, 3.0/6), Vector3(0.0/6, 3.0/6, -3.0/6), Vector3(1.0/6, 1.0/6, 4.0/6), Vector3(1.0/6, 4.0/6, 1.0/6), Vector3(3.0/6, -3.0/6, 0.0/6), Vector3(3.0/6, 0.0/6, -3.0/6), Vector3(4.0/6, 1.0/6, 1.0/6), },
                    { Vector3(-4.0/6, -1.0/6, -1.0/6), Vector3(-3.0/6, 0.0/6, 3.0/6), Vector3(-3.0/6, 3.0/6, 0.0/6), Vector3(-1.0/6, -4.0/6, -1.0/6), Vector3(-1.0/6, -1.0/6, -4.0/6), Vector3(0.0/6, -3.0/6, 3.0/6), Vector3(0.0/6, 3.0/6, -3.0/6), Vector3(0.0/6, 3.0/6, 3.0/6), Vector3(3.0/6, -3.0/6, 0.0/6), Vector3(3.0/6, 0.0/6, -3.0/6), Vector3(3.0/6, 0.0/6, 3.0/6), Vector3(3.0/6, 3.0/6, 0.0/6), },
                    { Vector3(-4.0/6, 1.0/6, 1.0/6), Vector3(-3.0/6, -3.0/6, 0.0/6), Vector3(-3.0/6, 0.0/6, -3.0/6), Vector3(-1.0/6, 1.0/6, 4.0/6), Vector3(-1.0/6, 4.0/6, 1.0/6), Vector3(0.0/6, -3.0/6, -3.0/6), Vector3(0.0/6, -3.0/6, 3.0/6), Vector3(0.0/6, 3.0/6, -3.0/6), Vector3(3.0/6, -3.0/6, 0.0/6), Vector3(3.0/6, 0.0/6, -3.0/6), Vector3(3.0/6, 0.0/6, 3.0/6), Vector3(3.0/6, 3.0/6, 0.0/6), },
                    { Vector3(-3.0/6, -3.0/6, 0.0/6), Vector3(-3.0/6, 0.0/6, -3.0/6), Vector3(-3.0/6, 0.0/6, 3.0/6), Vector3(-3.0/6, 3.0/6, 0.0/6), Vector3(0.0/6, -3.0/6, 3.0/6), Vector3(0.0/6, 3.0/6, -3.0/6), Vector3(0.0/6, 3.0/6, 3.0/6), Vector3(1.0/6, -4.0/6, -1.0/6), Vector3(1.0/6, -1.0/6, -4.0/6), Vector3(3.0/6, 0.0/6, 3.0/6), Vector3(3.0/6, 3.0/6, 0.0/6), Vector3(4.0/6, -1.0/6, -1.0/6), },
                    { Vector3(-3.0/6, -3.0/6, 0.0/6), Vector3(-3.0/6, 0.0/6, -3.0/6), Vector3(-3.0/6, 0.0/6, 3.0/6), Vector3(-3.0/6, 3.0/6, 0.0/6), Vector3(0.0/6, -3.0/6, -3.0/6), Vector3(0.0/6, 3.0/6, -3.0/6), Vector3(0.0/6, 3.0/6, 3.0/6), Vector3(1.0/6, -4.0/6, 1.0/6), Vector3(1.0/6, -1.0/6, 4.0/6), Vector3(3.0/6, 0.0/6, -3.0/6), Vector3(3.0/6, 3.0/6, 0.0/6), Vector3(4.0/6, -1.0/6, 1.0/6), },
                    { Vector3(-4.0/6, 1.0/6, -1.0/6), Vector3(-3.0/6, -3.0/6, 0.0/6), Vector3(-3.0/6, 0.0/6, 3.0/6), Vector3(-1.0/6, 1.0/6, -4.0/6), Vector3(-1.0/6, 4.0/6, -1.0/6), Vector3(0.0/6, -3.0/6, -3.0/6), Vector3(0.0/6, -3.0/6, 3.0/6), Vector3(0.0/6, 3.0/6, 3.0/6), Vector3(3.0/6, -3.0/6, 0.0/6), Vector3(3.0/6, 0.0/6, -3.0/6), Vector3(3.0/6, 0.0/6, 3.0/6), Vector3(3.0/6, 3.0/6, 0.0/6), },
                    { Vector3(-3.0/6, -3.0/6, 0.0/6), Vector3(-3.0/6, 0.0/6, -3.0/6), Vector3(-3.0/6, 0.0/6, 3.0/6), Vector3(-3.0/6, 3.0/6, 0.0/6), Vector3(0.0/6, -3.0/6, -3.0/6), Vector3(0.0/6, -3.0/6, 3.0/6), Vector3(0.0/6, 3.0/6, 3.0/6), Vector3(1.0/6, 1.0/6, -4.0/6), Vector3(1.0/6, 4.0/6, -1.0/6), Vector3(3.0/6, -3.0/6, 0.0/6), Vector3(3.0/6, 0.0/6, 3.0/6), Vector3(4.0/6, 1.0/6, -1.0/6), },
                    { Vector3(-4.0/6, -1.0/6, 1.0/6), Vector3(-3.0/6, 0.0/6, -3.0/6), Vector3(-3.0/6, 3.0/6, 0.0/6), Vector3(-1.0/6, -4.0/6, 1.0/6), Vector3(-1.0/6, -1.0/6, 4.0/6), Vector3(0.0/6, -3.0/6, -3.0/6), Vector3(0.0/6, 3.0/6, -3.0/6), Vector3(0.0/6, 3.0/6, 3.0/6), Vector3(3.0/6, -3.0/6, 0.0/6), Vector3(3.0/6, 0.0/6, -3.0/6), Vector3(3.0/6, 0.0/6, 3.0/6), Vector3(3.0/6, 3.0/6, 0.0/6), },
            };
            std::vector<LatticeVectorType> hcp_lattice_vectors(12);
            for(int i = 0; i < 8; i++) {
                for(int j = 0; j < 12; j++)
                    hcp_lattice_vectors[j] = latticeVectors.find(org_hcp_lattice_vectors[i][j]);
                std::sort(hcp_lattice_vectors.begin(), hcp_lattice_vectors.end());
                templateStructures.push_back(hcp_lattice_vectors);
            }
        }
        else if(latticeStructure == "fcc_perfect") {
            // Add FCC template.
            std::vector<LatticeVectorType> fcc_lattice_vectors(12);
            for(int i = 0; i < 12; i++) fcc_lattice_vectors[i] = i+1;
            templateStructures.push_back(fcc_lattice_vectors);
        }
    }
}

/******************************************************************************
* Is called by LAMMPS after the Fix has been created.
******************************************************************************/
void DislocationIdentificationFix::post_constructor()
{
    if(performSlipAnalysis) {
        // Create a new fix STORE style
        int n = strlen(id) + strlen("_LASTPOS_STORE") + 1;
        id_fix = new char[n];
        strcpy(id_fix,id);
        strcat(id_fix,"_LASTPOS_STORE");

        char** newarg = new char*[6];
        newarg[0] = id_fix;
        newarg[1] = group->names[igroup];
        newarg[2] = (char*)"STORE";
        newarg[3] = (char*)"peratom";
        newarg[4] = (char*)"1";
        newarg[5] = (char*)"4";
        modify->add_fix(6,newarg);
        lastpos_fix = static_cast<FixStore*>(modify->fix[modify->nfix-1]);
        delete[] newarg;

        // calculate xu,yu,zu for fix store array
        // skip if reset from restart file
        if(lastpos_fix->restart_reset) {
            lastpos_fix->restart_reset = 0;
        }
        else {
            if(debugOutput)
                printLog("Storing initial atomic positions.\n");

            double **x0 = lastpos_fix->astore;
            double **x = atom->x;
            imageint *image = atom->image;
            int nlocal = atom->nlocal;

            for(int i = 0; i < nlocal; i++) {
                domain->x2lamda(x[i], x0[i]);
                x0[i][3] = atom->tag[i];
            }
        }

        // Store initial simulation cell shape.
        for(int i = 0; i < 6; i++) {
            reference_h[i] = domain->h[i];
            reference_h_inv[i] = domain->h_inv[i];
        }
        reference_boxlo[0] = domain->boxlo[0];
        reference_boxlo[1] = domain->boxlo[1];
        reference_boxlo[2] = domain->boxlo[2];
    }
}

/******************************************************************************
* Destructor.
******************************************************************************/
DislocationIdentificationFix::~DislocationIdentificationFix()
{
    // Check nfix in case all fixes have already been deleted.
    if(modify->nfix && lastpos_fix != NULL) modify->delete_fix(id_fix);
    delete[] id_fix;

    memory->destroy(displacements);
    memory->destroy(averagePositions);
}

/*********************************************************************
 * Precomputes the ideal lattice vectors.
 *********************************************************************/
void DislocationIdentificationFix::prepareLatticeVectors()
{
    // Put null vector at index 0 of vector table.
    latticeVectors.insert(latticeVectors.begin(), Vector3(0,0,0));
    isFullLatticeVector.insert(isFullLatticeVector.begin(), true);

    if(debugOutput)
        printLog("Initial number of lattice vectors: %i\n", latticeVectors.size());

    // Compute integer linear combinations of existing lattice vectors to generate more lattice vectors.
    double maxVectorLength = 0;

    // First, only generate full lattice vectors.
    size_t lvcount = latticeVectors.size();
    for(size_t i = 1; i < lvcount; i++) {
        if(!isFullLatticeVector[i]) continue;
        for(size_t j = 1; j <= i; j++) {
            if(!isFullLatticeVector[j]) continue;
            Vector3 v = latticeVectors[i] + latticeVectors[j];
            if(latticeVectors.find(v) == -1) {
                latticeVectors.push_back(v);
                isFullLatticeVector.push_back(true);
                maxVectorLength = std::max(maxVectorLength, v.squared_length());
            }
        }
    }

    // Now also generate fractional lattice vectors.
    for(size_t i = 1; i < lvcount; i++) {
        for(size_t j = 1; j <= i; j++) {
            Vector3 v = latticeVectors[i] + latticeVectors[j];
            if(latticeVectors.find(v) == -1) {
                latticeVectors.push_back(v);
                isFullLatticeVector.push_back(isFullLatticeVector[i] && isFullLatticeVector[j]);
                maxVectorLength = std::max(maxVectorLength, v.squared_length());
            }
        }
    }

    // Extend max vector length.
    maxVectorLength *= 1.2;

    // Consider the lattice vectors we have generated so far as primary vectors.
    primaryLatticeVectorCount = latticeVectors.size();

    // Now generate more vectors, which we consider secondary lattice vectors.
    for(int depth = 0; depth < 1; depth++) {

        // First, only generate full lattice vectors.
        size_t lvcount = latticeVectors.size();
        for(size_t i = 1; i < lvcount; i++) {
            if(!isFullLatticeVector[i]) continue;
            for(size_t j = 1; j <= i; j++) {
                if(!isFullLatticeVector[j]) continue;
                Vector3 v = latticeVectors[i] + latticeVectors[j];
                if(v.squared_length() <= maxVectorLength && latticeVectors.find(v) == -1) {
                    latticeVectors.push_back(v);
                    isFullLatticeVector.push_back(true);
                }
            }
        }

        // Now also generate fractional lattice vectors.
        for(size_t i = 1; i < lvcount; i++) {
            for(size_t j = 1; j <= i; j++) {
                Vector3 v = latticeVectors[i] + latticeVectors[j];
                if(v.squared_length() <= maxVectorLength && latticeVectors.find(v) == -1) {
                    latticeVectors.push_back(v);
                    isFullLatticeVector.push_back(isFullLatticeVector[i] && isFullLatticeVector[j]);
                }
            }
        }
    }

    if(debugOutput) {
        printLog("Final number of primary lattice vectors: %i\n", primaryLatticeVectorCount);
        printLog("Final number of lattice vectors: %i\n", latticeVectors.size());
        printLog("Maximum length of primary lattice vectors: %f\n", sqrt(maxVectorLength));
        printLog("Vector sum lookup table size: %i\n", latticeVectors.size()*latticeVectors.size());
    }

    // Generate lookup table for reverse vectors.
    reverseLatticeVectors.resize(latticeVectors.size(), -1);
    for(size_t i = 0; i < latticeVectors.size(); i++) {
        if(reverseLatticeVectors[i] == -1) {
            reverseLatticeVectors[i] = latticeVectors.find(-latticeVectors[i]);
            reverseLatticeVectors[reverseLatticeVectors[i]] = i;
        }
    }

    // Build lookup table for vector sums.
    latticeVectorSums.resize(latticeVectors.size());
    for(size_t i = 0; i < latticeVectors.size(); i++) {
        latticeVectorSums[i].resize(latticeVectors.size(), -2);
    }
    for(size_t i = 0; i < latticeVectors.size(); i++) {
        for(size_t j = 0; j <= i; j++) {
            if(latticeVectorSums[i][j] == -2) {
                latticeVectorSums[j][i] = latticeVectorSums[i][j] = latticeVectors.find(latticeVectors[i] + latticeVectors[j]);
                LatticeVectorType irev = reverseLatticeVectors[i];
                LatticeVectorType jrev = reverseLatticeVectors[j];
                if(latticeVectorSums[i][j] >= 0) {
                    latticeVectorSums[jrev][irev] = latticeVectorSums[irev][jrev] = reverseLatticeVectors[latticeVectorSums[i][j]];
                }
                else {
                    latticeVectorSums[jrev][irev] = latticeVectorSums[irev][jrev] = -1;
                }
            }
        }
    }

    // Is the selected data type sufficient to store all lattice vectors?
    if(latticeVectors.size() > (size_t)std::numeric_limits<LatticeVectorType>::max())
        error->all(FLERR,"Number of lattice vectors exceeds data type range.");

    assert(isFullLatticeVector.size() == latticeVectors.size());

#if 0
    // Determine stacking fault vector corresponding to fractional lattice vectors.
    stackingFaultVectors.resize(latticeVectors.size(), 0);
    for(size_t i = 1; i < stackingFaultVectors.size(); i++) {
        if(!isFullLatticeVector[i]) {
            double shortestVectorLength = 2 * maxVectorLength;
            for(size_t j = 1; j < latticeVectors.size(); j++) {
                if(isFullLatticeVector[j]) {
                    Vector3 v = latticeVectors[i] + latticeVectors[j];
                    LatticeVectorType sfvec = latticeVectors.find(v);
                    if(sfvec != -1) {
                        double l = v.squared_length();
                        if(l < shortestVectorLength - 1e-12 || (l < shortestVectorLength + 1e-12 && std::min(sfvec,reverseLatticeVectors[sfvec]) < std::min(stackingFaultVectors[i],reverseLatticeVectors[stackingFaultVectors[i]]))) {
                            shortestVectorLength = l;
                            stackingFaultVectors[i] = sfvec;
                        }
                    }
                }
            }
        }
    }
#endif

    if(debugOutput) {
        printLog("Done with preparing lattice vectors.\n");
    }
}

/*********************************************************************
 * Precomputes the rotated lattice vectors.
 *********************************************************************/
void DislocationIdentificationFix::prepareRotatedLatticeVectors(double orientation[3][3])
{
    // Check if coordinate axes are orthogonal.
    if(orientation[0][0]*orientation[1][0] + orientation[0][1]*orientation[1][1] + orientation[0][2]*orientation[1][2] ||
        orientation[1][0]*orientation[2][0] + orientation[1][1]*orientation[2][1] + orientation[1][2]*orientation[2][2] ||
        orientation[0][0]*orientation[2][0] + orientation[0][1]*orientation[2][1] + orientation[0][2]*orientation[2][2])
        error->all(FLERR,"Lattice orient vectors are not orthogonal.");

    // Check righthandedness of orientation vectors. x cross y must be in same direction as z.
    int xy0 = orientation[0][1]*orientation[1][2] - orientation[0][2]*orientation[1][1];
    int xy1 = orientation[0][2]*orientation[1][0] - orientation[0][0]*orientation[1][2];
    int xy2 = orientation[0][0]*orientation[1][1] - orientation[0][1]*orientation[1][0];
    if(xy0*orientation[2][0] + xy1*orientation[2][1] + xy2*orientation[2][2] <= 0)
        error->all(FLERR,"Lattice orient vectors are not right-handed.");

    // Normalize orient vectors.
    for(int i = 0; i < 3; i++) {
        double len = sqrt(orientation[i][0]*orientation[i][0] + orientation[i][1]*orientation[i][1] + orientation[i][2]*orientation[i][2]);
        for(int j = 0; j < 3; j++) orientation[i][j] /= len;
    }

    // Prepare vector transformation matrix.
    for(int i = 0; i < 3; i++)
        for(int j = 0; j < 3; j++)
            lattice_rotation[i][j] = orientation[i][j] / latticeParameter;

    // Prepare lattice orientation matrix.
    double det =((orientation[0][0]*orientation[1][1] - orientation[0][1]*orientation[1][0])*(orientation[2][2])
              -(orientation[0][0]*orientation[1][2] - orientation[0][2]*orientation[1][0])*(orientation[2][1])
              +(orientation[0][1]*orientation[1][2] - orientation[0][2]*orientation[1][1])*(orientation[2][0]));

    inv_lattice_rotation[0][0] = (orientation[1][1]*orientation[2][2] - orientation[1][2]*orientation[2][1])/det*latticeParameter;
    inv_lattice_rotation[0][1] = (orientation[2][0]*orientation[1][2] - orientation[1][0]*orientation[2][2])/det*latticeParameter;
    inv_lattice_rotation[0][2] = (orientation[1][0]*orientation[2][1] - orientation[1][1]*orientation[2][0])/det*latticeParameter;
    inv_lattice_rotation[1][0] = (orientation[2][1]*orientation[0][2] - orientation[0][1]*orientation[2][2])/det*latticeParameter;
    inv_lattice_rotation[1][1] = (orientation[0][0]*orientation[2][2] - orientation[2][0]*orientation[0][2])/det*latticeParameter;
    inv_lattice_rotation[1][2] = (orientation[0][1]*orientation[2][0] - orientation[0][0]*orientation[2][1])/det*latticeParameter;
    inv_lattice_rotation[2][0] = (orientation[0][1]*orientation[1][2] - orientation[1][1]*orientation[0][2])/det*latticeParameter;
    inv_lattice_rotation[2][1] = (orientation[0][2]*orientation[1][0] - orientation[0][0]*orientation[1][2])/det*latticeParameter;
    inv_lattice_rotation[2][2] = (orientation[0][0]*orientation[1][1] - orientation[1][0]*orientation[0][1])/det*latticeParameter;

    // Transform lattice vectors and scale by the user-specified lattice constant.
    scaledLatticeVectors.resize(latticeVectors.size());
    for(size_t i = 0; i < latticeVectors.size(); i++) {
        for(size_t a = 0; a < 3; a++) {
            scaledLatticeVectors[i][a] = 0;
            for(size_t b = 0; b < 3; b++)
                scaledLatticeVectors[i][a] += inv_lattice_rotation[a][b] * latticeVectors[i][b];
        }
    }
}

/*********************************************************************
 * The return value of this method specifies at which points the
 * fix is invoked during the simulation.
 *********************************************************************/
int DislocationIdentificationFix::setmask()
{
    return FixConst::END_OF_STEP;
}

/*********************************************************************
 * This gets called by the system before the simulation starts.
 *********************************************************************/
void DislocationIdentificationFix::init()
{
    if(atom->tag_enable == 0)
        error->all(FLERR, "Fix disloc requires atoms having IDs. Please use 'atom_modify id yes'.");

    if(performSlipAnalysis) {
        // Set fix which stores previous atom coords
        int ifix = modify->find_fix(id_fix);
        if(ifix < 0) error->all(FLERR, "Could not find fix disloc internal storage fix ID");
        lastpos_fix = static_cast<FixStore*>(modify->fix[ifix]);
    }

    if(!crystalorientComputeName.empty()) {
        int icompute = modify->find_compute(crystalorientComputeName.c_str());
        if(icompute < 0)
            error->all(FLERR,"Compute ID for fix disloc does not exist");
        crystalorient_compute = modify->compute[icompute];
        if(crystalorient_compute->vector_flag == 0)
            error->all(FLERR,"Fix disloc orientation compute does not calculate a vector");
        if(crystalorient_compute->size_vector != 10)
            error->all(FLERR,"Fix disloc orientation compute does not calculate a vector of the right size");
    }

    // Need an occasional full neighbor list
    int irequest = neighbor->request(this,instance_me);
    neighbor->requests[irequest]->pair = 0;
    neighbor->requests[irequest]->compute = 0;
    neighbor->requests[irequest]->fix = 1;
    neighbor->requests[irequest]->half = 0;
    neighbor->requests[irequest]->full = 1;
    neighbor->requests[irequest]->occasional = 1;
}

/*********************************************************************
 * Initialize one atom's storage values, called when atom is created
 *********************************************************************/
void DislocationIdentificationFix::set_arrays(int i)
{
    if(lastpos_fix) {
        double **x0 = lastpos_fix->astore;
        domain->x2lamda(atom->x[i], x0[i]);
    }
}

/*********************************************************************
 * This is called by LAMMPS at the beginning of a run.
 *********************************************************************/
void DislocationIdentificationFix::setup(int)
{
    // Check if ghost layer is thick enough.
    double ghostLayerSize;
    if(domain->triclinic) {
        // cutghost is in lambda coordinates for triclinic boxes, use subxx_lamda
        double* h = domain->h;
        double cut_bound[3];
        cut_bound[0] = h[0]*comm->cutghost[0] + h[5]*comm->cutghost[1] + h[4]*comm->cutghost[2];
        cut_bound[1] = h[1]*comm->cutghost[1] + h[3]*comm->cutghost[2];
        cut_bound[2] = h[2]*comm->cutghost[2];
        ghostLayerSize = std::min(cut_bound[0], std::min(cut_bound[1], cut_bound[2]));
    }
    else {
        ghostLayerSize = std::min(comm->cutghost[0], std::min(comm->cutghost[1], comm->cutghost[2]));
    }

    printLog("Code version: %s\n", FIX_DISLOC_CODE_VERSION);
    printLog("Atomic structure identification: %s\n", useAtomicStructureIdentification ? "on" : "off");
    printLog("Current ghost layer size: %f\n", ghostLayerSize);
    printLog("Neighbor skin distance: %f\n", neighbor->skin);
    printLog("Effective ghost layer size: %f\n", ghostLayerSize - neighbor->skin);
    printLog("Interatomic distance: %f\n", sqrt(scaledLatticeVectors[1].squared_length()));
    printLog("Min. required effective ghost layer size: %f\n", sqrt(scaledLatticeVectors[1].squared_length() * 8.0));
    printLog("Min. required ghost layer size: %f\n", sqrt(scaledLatticeVectors[1].squared_length() * 8.0) + neighbor->skin);
    if((ghostLayerSize - neighbor->skin) <= sqrt(scaledLatticeVectors[1].squared_length() * 8.0))
        error->all(FLERR,"Ghost layer size is too small to perform dislocation identification.");

    if(performAnalysisAtBeginningOfRun || useStaticTessellation) {
        // Perform a first analysis step at the beginning of a run.
        end_of_step();
    }
}

/*********************************************************************
 * Called by LAMMPS at the end of each selected MD integration step.
 *********************************************************************/
void DislocationIdentificationFix::end_of_step()
{
    if(debugOutput)
        printLog("Performing dislocation analysis on timestep %i\n", update->ntimestep);

    // Discard data from last analysis run.
    if(!useStaticTessellation) {
        tessellation.reset();
        edgePool.clear();
        vertexEdges.clear();
        cellEdges.clear();
    }

    if(averageFix) {
        // Check if average positions are available on the current timestep.
        if(update->ntimestep % averageFix->peratom_freq)
            error->all(FLERR,"Fix ave/atom used by fix disloc is not computed at compatible timestep");

        // Calculate time-averaged atomic displacements for local atoms.
        memory->grow(averagePositions,atom->nmax,3,"fix_disloc:averagePositions");
        const double *h = domain->h;
        for(int i = 0; i < atom->nlocal; i++) {
            int xbox = (atom->image[i] & IMGMASK) - IMGMAX;
            int ybox = (atom->image[i] >> IMGBITS & IMGMASK) - IMGMAX;
            int zbox = (atom->image[i] >> IMG2BITS) - IMGMAX;

            double absx, absy, absz;
            if(domain->triclinic) {
                absx = atom->x[i][0] + h[0]*xbox + h[5]*ybox + h[4]*zbox;
                absy = atom->x[i][1] + h[1]*ybox + h[3]*zbox;
                absz = atom->x[i][2] + h[2]*zbox;
            }
            else {
                absx = atom->x[i][0] + domain->xprd*xbox;
                absy = atom->x[i][1] + domain->yprd*ybox;
                absz = atom->x[i][2] + domain->zprd*zbox;
            }
            averagePositions[i][0] = averageFix->array_atom[i][0] - absx;
            averagePositions[i][1] = averageFix->array_atom[i][1] - absy;
            averagePositions[i][2] = averageFix->array_atom[i][2] - absz;
        }

        // Exchange time-averaged displacements with neighboring procs.
        commStage = 3;
        comm->forward_comm_fix(this);

        // Compute time-averaged absolute positions.
        for(int i = 0; i < atom->nlocal + atom->nghost; i++) {
            averagePositions[i][0] += atom->x[i][0];
            averagePositions[i][1] += atom->x[i][1];
            averagePositions[i][2] += atom->x[i][2];
        }
    }

    // Determine current lattice orientation.
    if(crystalorient_compute) {
        if(debugOutput)
            printLog("Querying current lattice orientation from compute crystalorient\n");
        if(!(crystalorient_compute->invoked_flag & INVOKED_VECTOR)) {
            crystalorient_compute->compute_vector();
            crystalorient_compute->invoked_flag |= INVOKED_VECTOR;
        }
        for(int i = 0; i < 3; i++)
            for(int j = 0; j < 3; j++)
                inv_lattice_rotation[i][j] = crystalorient_compute->vector[i*3+j] * latticeParameter;

        // Transform lattice vectors and scale by the user-specified lattice constant.
        scaledLatticeVectors.resize(latticeVectors.size());
        for(size_t i = 0; i < latticeVectors.size(); i++) {
            for(size_t a = 0; a < 3; a++) {
                scaledLatticeVectors[i][a] = 0;
                for(size_t b = 0; b < 3; b++)
                    scaledLatticeVectors[i][a] += inv_lattice_rotation[a][b] * latticeVectors[i][b];
            }
        }

        // When working with a static tessellation, determine the lattice orientation only once at the beginning of the MD run.
        if(useStaticTessellation) {
            crystalorient_compute = NULL;
        }
    }

    // Construct the Delaunay tessellation based on the current atomic positions.
    if(!useStaticTessellation || !tessellation) {
        generateTessellation();

        // Assign initial lattice vectors to tessellation edges.
        assignLatticeVectors(false);

        // Identify local atomic structures.
        if(useAtomicStructureIdentification) {
            identifyAtomicStructure(false);
        }

        // Transfer lattice vectors between processors.
        synchronizeEdgeVectors(false);

        if(optimizeLatticeVectorsEnabled || eliminateSmallLoopsEnabled || eliminateLineLengthEnabled) {
            for(EdgeMemoryPool<>::iterator edge = edgePool.begin(); edge != edgePool.end(); ++edge) {
                // Reset edge flags.
                edge->setFlagged();
                // Make a copy of the original lattice vectors so that we can detect later which ones have been modified.
                edge->initialLatticeVector = edge->latticeVector;
            }
        }

        // Optimize assigned lattice vectors to reduce artifacts.
        if(optimizeLatticeVectorsEnabled) {
            optimizeAssignedLatticeVectors();
        }

        for(int iteration = 0; iteration < 2; iteration++) {

            // Modify the elastic field such that unnecessary dislocations are eliminated.
            if(eliminateLineLengthEnabled) {
                eliminateLineLength();
            }

            // Optimize assigned lattice vectors to avoid artifacts.
            if(eliminateSmallLoopsEnabled) {
                eliminateSmallLoops();
            }

            if(optimizeLatticeVectorsEnabled || eliminateSmallLoopsEnabled || eliminateLineLengthEnabled) {
                // Transfer lattice vectors of edges between processors.
                synchronizeEdgeVectors(true);
            }
        }

        // Transfer lattice vectors of edges between processors.
        synchronizeEdgeVectors(false);
    }

    // Compute elastic mapping in deformed configuration.
    if(performSlipAnalysis) {

        // Compute the atomic displacement vectors.
        computeDisplacements();

        // Store copy of lattice vectors and reset edge flags.
        for(EdgeMemoryPool<>::iterator edge = edgePool.begin(); edge != edgePool.end(); ++edge) {
            edge->deformedLatticeVector = edge->latticeVector;
            //if(edge->isInitiallyFixed())
                edge->latticeVector = -1;
            edge->unsetIsInitiallyFixed();
            edge->unsetIsFixed();
            edge->unsetFlagged();
        }

        // Assign initial lattice vectors to tessellation edges.
        assignLatticeVectors(true);

        // Identify local atomic structure in changed configuration.
        if(useAtomicStructureIdentification) {
            identifyAtomicStructure(true);
        }

        // Transfer lattice vectors between processors.
        synchronizeEdgeVectors(false);

        if(optimizeLatticeVectorsEnabled || eliminateSmallLoopsEnabled || eliminateLineLengthEnabled) {
            for(EdgeMemoryPool<>::iterator edge = edgePool.begin(); edge != edgePool.end(); ++edge) {
                // Reset edge flags.
                edge->setFlagged();
                // Make a copy of the original lattice vectors so that we can detect which ones have been changed later.
                edge->initialLatticeVector = edge->latticeVector;
            }
        }

        for(int iteration = 0; iteration < 2; iteration++) {

            // Modify the elastic field such that unnecessary dislocations are eliminated.
            if(eliminateLineLengthEnabled) {
                eliminateLineLength();
            }

            // Optimize assigned lattice vectors to avoid artifacts.
            if(eliminateSmallLoopsEnabled) {
                eliminateSmallLoops();
            }

            if(optimizeLatticeVectorsEnabled || eliminateSmallLoopsEnabled || eliminateLineLengthEnabled) {
                // Transfer lattice vectors of edges between processors.
                synchronizeEdgeVectors(true);
            }
        }

        // Eliminate slip surface artifacts.
        eliminateSlipSurfaceArtifacts();

        // Transfer lattice vectors of edges between processors.
        synchronizeEdgeVectors(false);

        // Swap back current and deformed lattice vectors.
        for(EdgeMemoryPool<>::iterator edge = edgePool.begin(); edge != edgePool.end(); ++edge) {
            std::swap(edge->latticeVector, edge->deformedLatticeVector);
        }

        // Computes how much plastic slip ocurred on each slip system
        // and the total plastic displacement gradient.
        computeSweptAreaPerSlipSystem();
    }

    // Dump dislocations and other information to output file.
    if(!outputBasename.empty()) {

        // Determine output filename by replacing '*' with the current timestep.
        std::string pathname = outputBasename;
        size_t wildcardPos = pathname.find('*');
        if(wildcardPos != std::string::npos) {
            char time_str[16];
            sprintf(time_str, "%i", (int)update->ntimestep);
            pathname.replace(wildcardPos, 1, time_str);
        }

        if(debugOutput)
            printLog("Writing disloc output file (parallel I/O mode: %s): %s\n", FIX_DISLOC_USE_PARALLEL_IO ? "on" : "off", pathname.c_str());

        // Write dislocation output file.
        writeOutputFile(pathname);
    }

#if 0
    printLog("Writing debug output.\n");

    // Dump data to disk for debugging purposes.
    std::ostringstream fn1;
    fn1 << "tessellation." << comm->me << ".data";
    dumpTessellationToDataFile(fn1.str());

    std::ostringstream fn2;
    fn2 << "dislocated_facets.new." << comm->me << ".vtk";
    dumpDislocatedFacets(fn2.str());
#endif

    if(debugOutput)
        printLog("Finished dislocation analysis on timestep %i\n", update->ntimestep);
}

/*********************************************************************
 * Computes the atomic displacement vectors.
 *********************************************************************/
void DislocationIdentificationFix::computeDisplacements()
{
    if(debugOutput)
        printLog("Computing displacement vectors.\n");

    // Grow local displacement array if necessary
    if(atom->nmax > nmaxDisplacements) {
        memory->destroy(displacements);
        nmaxDisplacements = atom->nmax;
        memory->create(displacements,nmaxDisplacements,"disloc:displacements");
    }

    double **x0 = lastpos_fix->astore;
    double **x = atom->x;
    imageint *image = atom->image;
    int nlocal = atom->nlocal;

    for(int i = 0; i < nlocal; i++) {
        double reduced_new[3];
        double reduced_old[3] = { x0[i][0], x0[i][1], x0[i][2] };
        domain->x2lamda(x[i], reduced_new, reference_boxlo, reference_h_inv);
        if(domain->xperiodic)
            reduced_old[0] += floor(reduced_new[0] - reduced_old[0] + 0.5);
        if(domain->yperiodic)
            reduced_old[1] += floor(reduced_new[1] - reduced_old[1] + 0.5);
        if(domain->zperiodic)
            reduced_old[2] += floor(reduced_new[2] - reduced_old[2] + 0.5);

        double oldpos[3];
        oldpos[0] = reference_h[0]*reduced_old[0] + reference_h[5]*reduced_old[1] + reference_h[4]*reduced_old[2] + reference_boxlo[0];
        oldpos[1] = reference_h[1]*reduced_old[1] + reference_h[3]*reduced_old[2] + reference_boxlo[1];
        oldpos[2] = reference_h[2]*reduced_old[2] + reference_boxlo[2];

        double newpos[3];
        newpos[0] = x[i][0];
        newpos[1] = x[i][1];
        newpos[2] = x[i][2];

        displacements[i][0] = newpos[0] - oldpos[0];
        displacements[i][1] = newpos[1] - oldpos[1];
        displacements[i][2] = newpos[2] - oldpos[2];

        // Update stored positions.
        if(incrementalSlipAnalysis) {
            domain->x2lamda(x[i], x0[i]);
        }
    }

    // Communicate displacements of ghost atoms.
    commStage = 1;
    comm->forward_comm_fix(this);

    if(incrementalSlipAnalysis) {
        // Updated stored shape of initial simulation cell.
        for(int i = 0; i < 6; i++) {
            reference_h[i] = domain->h[i];
            reference_h_inv[i] = domain->h_inv[i];
        }
        reference_boxlo[0] = domain->boxlo[0];
        reference_boxlo[1] = domain->boxlo[1];
        reference_boxlo[2] = domain->boxlo[2];
    }
}

/*********************************************************************
 * Calls the GEOGRAM library to compute the Delaunay tessellation.
 * The atomic coordinates serve as input vertices for the tessellation.
 *********************************************************************/
void DislocationIdentificationFix::generateTessellation()
{
    if(debugOutput) {
        printLog("Number of local atoms: %i\n", atom->nlocal);
        printLog("Number of ghost atoms: %i\n", atom->nghost);
    }

    // Initialize the Geogram library.
	GEO::initialize();
	GEO::set_assert_mode(GEO::ASSERT_ABORT);

    // Create the internal Delaunay generator object.
	tessellation = GEO::Delaunay::create(3, "BDEL");
    tessellation->set_keeps_infinite(true);
    tessellation->set_reorder(true);

    // Grow storage array if necessary.
    if(atom->nmax > nmaxDisplacements) {
        memory->destroy(displacements);
        nmaxDisplacements = atom->nmax;
        memory->create(displacements, nmaxDisplacements, "disloc:displacements");
    }

    // Generate small random pertubations of atomic positions.
    int nlocal = atom->nlocal;
    RanPark* localRandom = new RanPark(lmp, 1 + universe->me);
    for(int i = 0; i < nlocal; i++) {
        for(int j = 0; j < 3; j++)
            displacements[i][j] = 1e-9 * latticeParameter * localRandom->uniform();
    }
    delete localRandom;

    // Communicate pertubations of ghost atoms.
    commStage = 0;
    comm->forward_comm_fix(this);

    double **x = atom->x;
    if(averagePositions != NULL)
        x = averagePositions;

    // Apply pertubations to atomic positions.
    int nghost = atom->nghost;
    for(int i = 0; i < nlocal + nghost; i++) {
        for(int j = 0; j < 3; j++)
            displacements[i][j] += x[i][j];
    }

    // Construct Delaunay tessellation.
    tessellation->set_vertices(nlocal + nghost, &displacements[0][0]);

    if(debugOutput)
        printLog("Number of tessellation cells on local proc: %i\n", tessellation->nb_cells());

    // Extracts the edges of the Delaunay tessellation.
    createTessellationEdges();
}

/*********************************************************************
 * Extracts the edges of the Delaunay tessellation.
 *********************************************************************/
void DislocationIdentificationFix::createTessellationEdges()
{
    if(debugOutput)
        printLog("Creating tessellation edges.\n");

    // List of six edges of a tetrahedron.
    // Each edge is defined in terms of two cell vertex indices.
    static const int edgeVertices[6][2] = {{0,1},{0,2},{0,3},{1,2},{1,3},{2,3}};

    static const int tab_next_around_edge[4][4] = {
                      {5, 2, 3, 1},
                      {3, 5, 0, 2},
                      {1, 3, 5, 0},
                      {2, 0, 1, 5} };

    size_t numEdges = 0;
    size_t numLocalEdges = 0;

    // Allocate vertex and cell arrays.
    vertexEdges.resize(atom->nlocal + atom->nghost, NULL);
    cellEdges.resize(tessellation->nb_cells());

    for(GEO::index_t cell = 0; cell < tessellation->nb_cells(); ++cell) {

        if(!isFiniteCell(cell))
            continue;

        for(int edgeIndex = 0; edgeIndex < 6; edgeIndex++) {
            GEO::index_t v1 = tessellation->cell_vertex(cell, edgeVertices[edgeIndex][0]);
            GEO::index_t v2 = tessellation->cell_vertex(cell, edgeVertices[edgeIndex][1]);
            Edge* edge = findEdge(v1, v2);
            if(!edge) {
                edge = edgePool.construct(v2, false);
                Edge* oppositeEdge = edgePool.construct(v1, true);

                // Insert edge and its opposite edge into the linked list of
                // edges of the vertices they connect.
                edge->nextLeavingEdge = vertexEdges[v1];
                vertexEdges[v1] = edge;
                oppositeEdge->nextLeavingEdge = vertexEdges[v2];
                vertexEdges[v2] = oppositeEdge;

                edge->incidentFacet = Facet(cell, tab_next_around_edge[edgeVertices[edgeIndex][0]][edgeVertices[edgeIndex][1]]);
                oppositeEdge->incidentFacet = Facet(cell, tab_next_around_edge[edgeVertices[edgeIndex][1]][edgeVertices[edgeIndex][0]]);

                numEdges++;

                // Determine whether this Delaunay edge is owned by the local processor or a neighboring proc.
                if((atom->tag[v1] < atom->tag[v2]) ? (v1 < atom->nlocal) : (v2 < atom->nlocal)) {
                    edge->setIsLocal();
                    oppositeEdge->setIsLocal();
                    numLocalEdges++;
                }
            }

            // Store pointer to edge in cell.
            cellEdges[cell].edges[edgeIndex] = edge;
        }
    }

    if(debugOutput) {
        printLog("Total number of tessellation edges: %i\n", numEdges);
        printLog("Number of local edges: %i\n", numLocalEdges);
        printLog("Number of ghost edges: %i\n", numEdges - numLocalEdges);
    }
}

/*********************************************************************
 * Assigns ideal lattice vectors to edges of the tessellation.
 *********************************************************************/
void DislocationIdentificationFix::assignLatticeVectors(bool displacedConfig)
{
    if(debugOutput)
        printLog("Assigning initial lattice vectors to Delaunay edges.\n");

    double** x = atom->x;
    if(averagePositions != NULL)
        x = averagePositions;

    for(int i = 0; i < atom->nlocal; i++) {
        for(Edge* edge = vertexEdges[i]; edge != NULL; edge = edge->nextLeavingEdge) {
            if(edge->isLocal() && edge->latticeVector == -1) {
                GEO::index_t v1 = edge->oppositeEdge()->vertex2;
                GEO::index_t v2 = edge->vertex2;
                Vector3 delta(x[v1][0] - x[v2][0], x[v1][1] - x[v2][1], x[v1][2] - x[v2][2]);
                if(displacedConfig) {
                    delta += displacements[v2] - displacements[v1];
                }
                LatticeVectorType lv = scaledLatticeVectors.closestPrimaryVector(delta, primaryLatticeVectorCount);
                if(lv != -1) {
                    edge->latticeVector = lv;
                    edge->oppositeEdge()->latticeVector = reverseLatticeVectors[lv];
                }
            }
        }
    }
}

// Pair of neighbor atoms that form a bond (bit-wise storage, i.e., two bits are set).
typedef unsigned int CNAPairBond;

/*
 A nearest neighbor of the central atom.
 Stores vector and squared distance.
 */
struct AdaptiveCNANeighbor {
    DislocationIdentificationFix::Vector3 delta;
    double distSq;
    int neighborIndex;
    DislocationIdentificationFix::LatticeVectorType latticeVector;

    // Used for sorting of neighbor lists.
    bool operator<(const AdaptiveCNANeighbor& o) const { return distSq < o.distSq; }
    bool operator>(const AdaptiveCNANeighbor& o) const { return distSq > o.distSq; }
};

static inline bool NeighborLatticeVectorCompare(const AdaptiveCNANeighbor& a, const AdaptiveCNANeighbor& b) {
    return (a.latticeVector < b.latticeVector);
}

static inline int findCommonNeighbors(const DislocationIdentificationFix::NeighborBondArray& neighborArray, int neighborIndex, unsigned int& commonNeighbors, int numNeighbors);
static inline int findNeighborBonds(const DislocationIdentificationFix::NeighborBondArray& neighborArray, unsigned int commonNeighbors, int numNeighbors, CNAPairBond* neighborBonds);
static inline int calcMaxChainLength(CNAPairBond* neighborBonds, int numBonds);

/// Fast sorting function for an array of (bounded) integers.
/// Sorts values in descending order.
template<typename iterator>
void bitmapSort(iterator begin, iterator end, int max)
{
    int bitarray = 0;
    for(iterator pin = begin; pin != end; ++pin) {
        bitarray |= 1 << (*pin);
    }
    iterator pout = begin;
    for(int i = max - 1; i >= 0; i--)
        if(bitarray & (1 << i))
            *pout++ = i;
}

/*********************************************************************
 * Identify local atomic structure.
 *********************************************************************/
void DislocationIdentificationFix::identifyAtomicStructure(bool displacedConfig)
{
    if(debugOutput) {
        printLog("Identifying atomic structures.\n");
        structureTypes.resize(atom->nlocal);
        std::fill(structureTypes.begin(), structureTypes.end(), 0);
    }

    // Invoke full neighbor list (will copy or build if necessary)
    neighbor->build_one(list);

    double** x = atom->x;
    if(averagePositions != NULL)
        x = averagePositions;

    const int inum = list->inum;
    const int* ilist = list->ilist;
    const int* numneigh = list->numneigh;
    int** firstneigh = list->firstneigh;

    // Perform adaptive CNA.
    std::vector<AdaptiveCNANeighbor> neighborVectors;
    std::vector<bigint> nmatches(templateStructures.size(), 0);
    for(int ii = 0; ii < inum; ii++) {
        int i = ilist[ii];

        Vector3 xyz(x[i][0], x[i][1], x[i][2]);
        if(displacedConfig)
            xyz -= displacements[i];

        int* jlist = firstneigh[i];
        int jnum = numneigh[i];

        // Ignore under-coordinated atoms.
        int nn = numLatticeStructureNeighbors;
        if(jnum < nn)
            continue;

        // Grow neighbor array if necessary.
        if(jnum > neighborVectors.size())
            neighborVectors.resize(jnum);

        // Generate full list of bond vectors.
        for(int jj = 0; jj < jnum; jj++) {
            int j = jlist[jj];
            j &= NEIGHMASK;

            AdaptiveCNANeighbor& n = neighborVectors[jj];
            for(size_t k = 0; k < 3; k++)
                n.delta[k] = xyz[k] - x[j][k];
            if(displacedConfig)
                n.delta += displacements[j];
            n.distSq = n.delta.squared_length();
            n.neighborIndex = j;
        }

        // Select N nearest neighbors from full list and sort them.
        std::partial_sort(neighborVectors.begin(), neighborVectors.begin() + nn, neighborVectors.begin() + jnum);

        // Compute scaling factor.
        double localScaling = 0;
        if(nn == 14) {
            // BCC:
            for(int n = 0; n < 8; n++)
                localScaling += sqrt(neighborVectors[n].distSq) / 0.866025404;
            for(int n = 8; n < 14; n++)
                localScaling += sqrt(neighborVectors[n].distSq);
        }
        else if(nn == 12) {
            // FCC:
            for(int n = 0; n < 12; n++)
                localScaling += sqrt(neighborVectors[n].distSq);
        }
        localScaling /= nn;
        double localCutoffSquared = localScaling * localScaling * (1.207106781 * 1.207106781);

        // Compute bond bit-flag array.
        NeighborBondArray neighborArray;
        for(int ni1 = 0; ni1 < nn; ni1++) {
            for(int ni2 = ni1+1; ni2 < nn; ni2++) {
                Vector3 del = neighborVectors[ni1].delta - neighborVectors[ni2].delta;
                if(del.squared_length() <= localCutoffSquared)
                    neighborArray.setNeighborBond(ni1, ni2);
            }
        }

        int templateStartIndex = 0;
        int templateEndIndex = templateStructures.size();
        if(nn == 14) {
            // BCC:
            int n444 = 0;
            int n666 = 0;
            for(int ni = 0; ni < nn; ni++) {

                // Determine number of neighbors the two atoms have in common.
                unsigned int commonNeighbors;
                int numCommonNeighbors = findCommonNeighbors(neighborArray, ni, commonNeighbors, nn);
                if(numCommonNeighbors != 4 && numCommonNeighbors != 6)
                    break;

                // Determine the number of bonds among the common neighbors.
                CNAPairBond neighborBonds[NeighborBondArray::CNA_MAX_PATTERN_NEIGHBORS*NeighborBondArray::CNA_MAX_PATTERN_NEIGHBORS];
                int numNeighborBonds = findNeighborBonds(neighborArray, commonNeighbors, nn, neighborBonds);
                if(numNeighborBonds != 4 && numNeighborBonds != 6)
                    break;

                // Determine the number of bonds in the longest continuous chain.
                int maxChainLength = calcMaxChainLength(neighborBonds, numNeighborBonds);
                if(numCommonNeighbors == 4 && numNeighborBonds == 4 && maxChainLength == 4) {
                    n444++;
                }
                else if(numCommonNeighbors == 6 && numNeighborBonds == 6 && maxChainLength == 6) {
                    n666++;
                }
                else break;
            }
            if(n666 != 8 || n444 != 6)
                continue;
        }
        else {
            // FCC/HCP:
            int n421 = 0;
            int n422 = 0;
            for(int ni = 0; ni < nn; ni++) {

                // Determine number of neighbors the two atoms have in common.
                unsigned int commonNeighbors;
                int numCommonNeighbors = findCommonNeighbors(neighborArray, ni, commonNeighbors, nn);
                if(numCommonNeighbors != 4)
                    break;

                // Determine the number of bonds among the common neighbors.
                CNAPairBond neighborBonds[NeighborBondArray::CNA_MAX_PATTERN_NEIGHBORS*NeighborBondArray::CNA_MAX_PATTERN_NEIGHBORS];
                int numNeighborBonds = findNeighborBonds(neighborArray, commonNeighbors, nn, neighborBonds);
                if(numNeighborBonds != 2)
                    break;

                // Determine the number of bonds in the longest continuous chain.
                int maxChainLength = calcMaxChainLength(neighborBonds, numNeighborBonds);
                if(maxChainLength == 1) {
                    n421++;
                }
                else if(maxChainLength == 2) {
                    n422++;
                }
                else {
                    break;
                }
            }
            if(n421 == 12) {
                templateStartIndex = 0;
                templateEndIndex = 5;
            }
            else if(n421 == 6 && n422 == 6) {
                templateStartIndex = 5;
                templateEndIndex = 13;
            }
            else {
                continue;
            }
        }

        int bestMatch = -1;
        double minDeviation = std::numeric_limits<double>::max();

        for(size_t templateIndex = templateStartIndex; templateIndex < templateEndIndex; templateIndex++) {
            const std::vector<LatticeVectorType>& templateVectors = templateStructures[templateIndex];

            // Map each of the neighbors to a lattice vector.
            double deviation = 0;
            for(int n = 0; n < templateVectors.size(); n++) {
                neighborVectors[n].latticeVector = scaledLatticeVectors.closestVectorFromSubset(neighborVectors[n].delta, templateVectors.data(), templateVectors.size());
                deviation += (neighborVectors[n].delta - scaledLatticeVectors[neighborVectors[n].latticeVector]).squared_length();
            }

            std::sort(neighborVectors.begin(), neighborVectors.begin() + templateVectors.size(), NeighborLatticeVectorCompare);
            bool match = true;
            for(int n = 0; n < templateVectors.size(); n++) {
                if(neighborVectors[n].latticeVector != templateVectors[n]) {
                    match = false;
                    break;
                }
            }

            if(match) {
                if(deviation < minDeviation) {
                    bestMatch = templateIndex;
                    minDeviation = deviation;
                    for(Edge* edge = vertexEdges[i]; edge != NULL; edge = edge->nextLeavingEdge) {
                        if(edge->isLocal()) {
                            for(int n = 0; n < templateVectors.size(); n++) {
                                if(neighborVectors[n].neighborIndex == edge->vertex2) {
                                    if(!edge->isFlagged()) {
                                        if(!edge->isInitiallyFixed() || edge->latticeVector == neighborVectors[n].latticeVector) {
                                            edge->setIsInitiallyFixed();
                                            edge->oppositeEdge()->setIsInitiallyFixed();
                                        }
                                        else {
                                            edge->unsetIsInitiallyFixed();
                                            edge->oppositeEdge()->unsetIsInitiallyFixed();
                                            edge->setFlagged();
                                            edge->oppositeEdge()->setFlagged();
                                        }
                                    }
                                    edge->latticeVector = neighborVectors[n].latticeVector;
                                    edge->oppositeEdge()->latticeVector = reverseLatticeVectors[edge->latticeVector];
                                    break;
                                }
                            }
                        }
                    }
                    for(int n = 0; n < templateVectors.size(); n++) {
                        if(neighborVectors[n].latticeVector == -1) continue;
                        for(Edge* edge = vertexEdges[neighborVectors[n].neighborIndex]; edge != NULL; edge = edge->nextLeavingEdge) {
                            if(edge->isLocal()) {
                                for(int n2 = 0; n2 < templateVectors.size(); n2++) {
                                    if(neighborVectors[n2].latticeVector == -1) continue;
                                    if(edge->vertex2 == neighborVectors[n2].neighborIndex) {
                                        LatticeVectorType lvsum = vectorSum(neighborVectors[n2].latticeVector, reverseLatticeVectors[neighborVectors[n].latticeVector]);
                                        if(lvsum != -1) {
                                            if(!edge->isFlagged()) {
                                                if(!edge->isInitiallyFixed() || edge->latticeVector == lvsum) {
                                                    edge->setIsInitiallyFixed();
                                                    edge->oppositeEdge()->setIsInitiallyFixed();
                                                }
                                                else {
                                                    edge->unsetIsInitiallyFixed();
                                                    edge->oppositeEdge()->unsetIsInitiallyFixed();
                                                    edge->setFlagged();
                                                    edge->oppositeEdge()->setFlagged();
                                                }
                                            }
                                            edge->latticeVector = lvsum;
                                            edge->oppositeEdge()->latticeVector = reverseLatticeVectors[lvsum];
                                        }
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        if(bestMatch != -1) {
            nmatches[bestMatch]++;
            if(!structureTypes.empty())
                structureTypes[i] = bestMatch+1;
        }
    }

    if(debugOutput) {
        std::vector<bigint> nmatches_global(nmatches.size());
        MPI_Allreduce(nmatches.data(), nmatches_global.data(), nmatches.size(), MPI_LMP_BIGINT, MPI_SUM, lmp->world);
        bigint totalMatches = std::accumulate(nmatches_global.begin(), nmatches_global.end(), (bigint)0);
        printLog("Total number of atomic structure matches: %i (%f %%)\n", totalMatches, (double)totalMatches/atom->natoms*100.0);
        for(size_t i = 0; i < nmatches_global.size(); i++) {
            printLog("  Matches for structure %i: %i (%f %%)\n", i, nmatches_global[i], (double)nmatches_global[i]/atom->natoms*100.0);
        }
    }
}

/*********************************************************************
 * Optimizes the lattice vectors assigned to tessellation edges
 * by eliminating as many dislocated triangles as possible.
 *********************************************************************/
void DislocationIdentificationFix::optimizeAssignedLatticeVectors()
{
    if(debugOutput)
        printLog("Optimizing assigned lattice vectors\n");

    bigint numFlippedEdgesSummed = 0;
    for(int optimizeIteration = 0; ; optimizeIteration++) {
        bigint numFlippedEdges = 0;

        std::vector< std::pair<LatticeVectorType,short> > vectorCounts;
        for(int pass = 1; pass <= 2; pass++) {
            for(EdgeMemoryPool<>::iterator edge = edgePool.begin(); edge != edgePool.end(); ++edge) {

                // Skip edges from a neighborhood that has remained unchanged during previous iteration.
                if(!edge->isFlagged()) continue;

                // Skip every other edge, because we want to visit each pair of edge/opposite edge only once.
                if(edge->isOddEdge()) continue;

                // Don't touch edges with an already fixed lattice vector.
                if(edge->isFixed() || edge->isInitiallyFixed()) continue;

                // Skip edges at the border of the domain which don't have any valid lattice vector.
                if(edge->latticeVector == -1) continue;

                // Give one initial vote to the existing lattice vector of the edge.
                vectorCounts.push_back(std::make_pair(edge->latticeVector, 1));

                GEO::index_t vertex1 = edge->oppositeEdge()->vertex2;
                GEO::index_t vertex2 = edge->vertex2;

                // Iterate over all triangles adjacent to the edge.
                FacetCirculator circulator_start = incident_facets(vertex1, vertex2, edge->incidentFacet.first, edge->incidentFacet.second);
                FacetCirculator circulator = circulator_start;
                do {
                    if(!isFiniteCell(circulator.cell())) {
                        --circulator;
                        continue;
                    }

                    // Look up the two other edges that form a triangle with the current edge.
                    Edge* edge1;
                    Edge* edge2;
                    int i = tessellation->index(circulator.cell(), vertex1);
                    int j = tessellation->index(circulator.cell(), vertex2);
                    LatticeVectorType lv = computeAlternativeEdgeLatticeVector(circulator.cell(), i, j, edge1, edge2);
                    if(lv != -1) {
                        assert(edge2->vertex2 == vertex2 && edge1->vertex2 == edge2->oppositeEdge()->vertex2 && edge1->oppositeEdge()->vertex2 == vertex1);

                        // If both adjacent edges have a fixed lattice vector,
                        // the lattice vector of the current edge will become fixed as well.
                        int vote = 1;
                        if(edge1->isFixedAny() && edge2->isFixedAny()) {
                            vote = 100;
                        }
                        else if(pass == 2 && (edge1->isFixedAny() || edge2->isFixedAny())) {
                            vote = 10;
                        }
                        else {
                            --circulator;
                            continue;
                        }

                        // Edge will have a fixed lattice vector after this pass.
                        edge->setIsFixed();
                        edge->oppositeEdge()->setIsFixed();

                        // Mark edge as unchanged for now.
                        edge->unsetFlagged();

                        // Increment vote count for the newly-computed lattice vector.
                        std::vector< std::pair<LatticeVectorType,short> >::iterator i = vectorCounts.begin();
                        for(; i != vectorCounts.end(); ++i) {
                            if(i->first == lv) {
                                i->second += vote;
                                break;
                            }
                        }
                        // If first vote for this lattice vector, extend vote vector:
                        if(i == vectorCounts.end())
                            vectorCounts.push_back(std::make_pair(lv, vote));
                    }
                    --circulator;
                }
                while(circulator != circulator_start);

                // Determine the lattice vector with the highest vote count.
                LatticeVectorType newLatticeVector = vectorCounts.front().first;
                short highestLatticeVectorCount = vectorCounts.front().second;
                for(std::vector< std::pair<LatticeVectorType,short> >::iterator i = vectorCounts.begin(); i != vectorCounts.end(); ++i) {
                    if(i->second > highestLatticeVectorCount || (i->second == highestLatticeVectorCount && i->first < newLatticeVector)) {
                        newLatticeVector = i->first;
                        highestLatticeVectorCount = i->second;
                    }
                }

                // Assign new edge vector.
                if(edge->latticeVector != newLatticeVector) {
                    numFlippedEdges++;
                    edge->latticeVector = newLatticeVector;
                    edge->oppositeEdge()->latticeVector = reverseLatticeVectors[newLatticeVector];

                    // Mark all edges in the neighborhood of the edge.
                    do {
                        if(isFiniteCell(circulator.cell())) {
                            // Look up the two other edges that form a triangle with the current edge.
                            Edge* edge1;
                            Edge* edge2;
                            int i = tessellation->index(circulator.cell(), vertex1);
                            int j = tessellation->index(circulator.cell(), vertex2);
                            computeAlternativeEdgeLatticeVector(circulator.cell(), i, j, edge1, edge2);
                            edge1->flagEdge();
                            edge2->flagEdge();
                        }
                        --circulator;
                    }
                    while(circulator != circulator_start);
                }
                vectorCounts.clear();
            }
        }
        numFlippedEdgesSummed += numFlippedEdges;

        if(debugOutput) {
            printLog(" Iteration %i (" BIGINT_FORMAT " local edge flips)\n", optimizeIteration+1, numFlippedEdges);
        }

        if(numFlippedEdges == 0) {
            // We are done. No more edges to optimize on this processor.
            if(debugOutput) {
                int maxNumIterations;
                MPI_Allreduce(&optimizeIteration, &maxNumIterations, 1, MPI_INT, MPI_MAX, lmp->world);
                bigint totalNumFlippedEdges;
                MPI_Allreduce(&numFlippedEdgesSummed, &totalNumFlippedEdges, 1, MPI_LMP_BIGINT, MPI_SUM, lmp->world);
                printLog("Performed " BIGINT_FORMAT " edge flips within %i iterations\n", totalNumFlippedEdges, maxNumIterations);
            }
            break;
        }
    }
}

/*********************************************************************
 * Computes the Burgers vector associated with a Delaunay facet.
 *********************************************************************/
DislocationIdentificationFix::LatticeVectorType DislocationIdentificationFix::computeFacetBurgersVector(Facet facet) const
{
    if(!isFiniteCell(facet.first))
        return -1;

    // Get the lattice vectors of the three edges of the triangle facet.
    LatticeVectorType lv[3];
    for(int i = 0; i < 3; i++) {
        lv[i] = cellEdges[facet.first].edges[burgersCircuitEdges[facet.second][i]]->latticeVector;
        if(lv[i] == -1) return -1;
    }

    // Sum up lattice vectors of the three edges.
    LatticeVectorType b = vectorSum(lv[0], lv[1]);
    if(b == -1) return -1;
    b = vectorSum(b, reverseLatticeVectors[lv[2]]);
    if(b == -1) return -1;

    // Correct sense for every other face.
    if(facet.second & 1)
        b = reverseLatticeVectors[b];

    return b;
}

/*********************************************************************
 * Modifies the elastic field such that small dislocation loops are
 * eliminated.
 *********************************************************************/
void DislocationIdentificationFix::eliminateSmallLoops()
{
    if(debugOutput)
        printLog("Eliminating small dislocation loops\n");

    std::vector<Edge*> edgeStack;

    // Reset edge flags.
    for(EdgeMemoryPool<>::iterator edge = edgePool.begin(); edge != edgePool.end(); ++edge)
        edge->unsetFlagged();

    // Iterate over all dislocated facets.
    for(GEO::index_t cell = 0; cell < tessellation->nb_cells(); ++cell) {
        if(!isFiniteCell(cell))
            continue;

        // Iterate over the four faces of the tetrahedron cell until we find a dislocated one.
        for(int f = 0; f < 4; f++) {

            // Perform Burgers circuit test on the facet.
            LatticeVectorType b = computeFacetBurgersVector(Facet(cell, f));
            if(b <= 0) continue;

            // Get the three edges of the face.
            Edge* facetEdges[3];
            for(int e = 0; e < 3; e++) {
                facetEdges[e] = cellEdges[cell].edges[burgersCircuitEdges[f][e]];
                if(e == 2) facetEdges[e] = facetEdges[e]->oppositeEdge();
            }

            for(int e = 0; e < 3; e++) {
                Edge* seedEdge = facetEdges[e];

                // Start only in the local processor domain.
                if(!seedEdge->isLocal()) continue;

                Edge* targetEdge = facetEdges[(e+1)%3]->oppositeEdge();

                bool isClosedLoop = true;

                // Start at this edge and recursively visit all adjacent edges sharing the same vertex 2.
                edgeStack.push_back(seedEdge);
                seedEdge->setFlagged();
                size_t currentEdgeIndex = 0;
                do {
                    Edge* currentEdge = edgeStack[currentEdgeIndex++];

                    // Loop over all adjacent facets of the current edge.
                    GEO::index_t vertex1 = currentEdge->oppositeEdge()->vertex2;
                    GEO::index_t vertex2 = currentEdge->vertex2;
                    FacetCirculator circulator_start = incident_facets(vertex1, vertex2, currentEdge->incidentFacet.first, currentEdge->incidentFacet.second);
                    FacetCirculator circulator = circulator_start;
                    do {
                        if(isFiniteCell(circulator.cell())) {
                            // Continue recursion only over non-dislocated facets.
                            if(computeFacetBurgersVector(Facet(circulator.cell(), circulator.facet())) == 0) {
                                // Put both edges of the facet which share the same vertex onto the recursion stack.
                                for(int j = 0; j < 3; j++) {
                                    Edge* facetEdge = cellEdges[circulator.cell()].edges[burgersCircuitEdges[circulator.facet()][j]];
                                    if(facetEdge->oppositeEdge()->vertex2 == seedEdge->vertex2)
                                        facetEdge = facetEdge->oppositeEdge();
                                    else if(facetEdge->vertex2 != seedEdge->vertex2)
                                        continue;
                                    if(facetEdge == targetEdge) {
                                        isClosedLoop = false;
                                        break;
                                    }
                                    if(!facetEdge->isFlagged()) {
                                        facetEdge->setFlagged();
                                        edgeStack.push_back(facetEdge);
                                    }
                                }
                            }
                        }
                        --circulator;
                    }
                    while(circulator != circulator_start && isClosedLoop);
                }
                while(currentEdgeIndex < edgeStack.size() && isClosedLoop);

                // Reset flags of edges visited during last pass.
                for(std::vector<Edge*>::const_iterator edge = edgeStack.begin(); edge != edgeStack.end(); ++edge)
                    (*edge)->unsetFlagged();

                if(isClosedLoop) {
                    // Apply a displacement to cut surface, which is equal to negative Burgers vector of the dislocation, to eliminate dislocation loop.
                    LatticeVectorType displacement = (f & 1) ? b : reverseLatticeVectors[b];
                    bool summationError = false;
                    for(std::vector<Edge*>::const_iterator edge = edgeStack.begin(); edge != edgeStack.end(); ++edge) {
                        LatticeVectorType newLatticeVec = vectorSum((*edge)->latticeVector, displacement);
                        if(newLatticeVec != -1) {
                            (*edge)->latticeVector = newLatticeVec;
                            (*edge)->oppositeEdge()->latticeVector = reverseLatticeVectors[newLatticeVec];
                        }
                        else {
                            summationError = true;
                            if(debugOutput) {
                                Vector3 sum = latticeVectors[(*edge)->latticeVector] + latticeVectors[displacement];
                                printLog("WARNING: Cannot add vectors %i (%f %f %f) and %i (%f %f %f) (sum magnitude: %f)\n", (*edge)->latticeVector, latticeVectors[(*edge)->latticeVector].x(), latticeVectors[(*edge)->latticeVector].y(), latticeVectors[(*edge)->latticeVector].z(),
                                    displacement,  latticeVectors[displacement].x(), latticeVectors[displacement].y(), latticeVectors[displacement].z(), sum.length());
                            }
                        }
                    }
                    assert(summationError || computeFacetBurgersVector(Facet(cell, f)) == 0);
                }

                edgeStack.clear();

                if(isClosedLoop)
                    break;
            }
        }
    }
}

/*********************************************************************
 * Modifies the elastic field such that unnecessary dislocations
 * are eliminated.
 *********************************************************************/
void DislocationIdentificationFix::eliminateLineLength()
{
    if(debugOutput)
        printLog("Eliminating unnecessary dislocation lines\n");

    // The maximum number of algorithm iterations to perform:
    const int maxIterations = 6;

    for(int iteration = 0; iteration < maxIterations; iteration++) {

        bigint nmodified = 0;
        for(EdgeMemoryPool<>::iterator edge = edgePool.begin(); edge != edgePool.end(); ++edge) {

            // Skip every other edge, because we want to visit each pair of edge/opposite edge only once.
            if(edge->isOddEdge()) continue;

            // Don't touch edges with a fixed lattice vector.
            if(edge->isInitiallyFixed()) continue;

            // Start only in the local processor domain.
            if(!edge->isLocal()) continue;

            // Skip edges at the border of the domain which don't have any valid lattice vector.
            if(edge->latticeVector == -1) continue;

            GEO::index_t vertex1 = edge->oppositeEdge()->vertex2;
            GEO::index_t vertex2 = edge->vertex2;

            std::pair<Edge*,LatticeVectorType> cut_surface[3];
            cut_surface[0].first = &*edge;
            cut_surface[1].first = NULL;
            int improvement = improveEdgeLatticeVectorRecursive(cut_surface, sizeof(cut_surface)/sizeof(cut_surface[0])-1);
            if(improvement > 0) {
                // Permanently apply cut-displacement operation in order to reduce total dislocation line length.
                for(int i = 0; i < sizeof(cut_surface)/sizeof(cut_surface[0]); i++) {
                    Edge* edge2 = cut_surface[i].first;
                    if(edge2 == NULL) break;
                    if(cut_surface[i].second != edge2->latticeVector) {
                        edge2->latticeVector = cut_surface[i].second;
                        edge2->oppositeEdge()->latticeVector = reverseLatticeVectors[edge2->latticeVector];
                        nmodified++;
                    }
                }
            }
        }

        bigint nmodified_total;
        MPI_Allreduce(&nmodified, &nmodified_total, 1, MPI_LMP_BIGINT, MPI_SUM, lmp->world);

        if(debugOutput)
            printLog(" Iteration %i: Modified " BIGINT_FORMAT " edges\n", iteration+1, nmodified_total);

        if(nmodified_total == 0)
            break;

        // Transfer lattice vectors of edges between processors.
        synchronizeEdgeVectors(true);
    }
}

/*********************************************************************
 * Modifies the lattice vector assigned to a Delaunay edge.
 *********************************************************************/
int DislocationIdentificationFix::improveEdgeLatticeVectorRecursive(std::pair<Edge*,LatticeVectorType>* results, int depth)
{
    Edge* edge = results[0].first;
    LatticeVectorType originalLatticeVector = edge->latticeVector;
    results[0].second = originalLatticeVector;
    if(edge->isInitiallyFixed() || originalLatticeVector == -1 || !edge->isLocal()) {
        results[0].first = NULL;
        return 0;
    }
    int bestImprovement = 0;
    LatticeVectorType bestNewLatticeVector = originalLatticeVector;
    LatticeVectorType previous_b = 0;
    GEO::index_t vertex1 = edge->oppositeEdge()->vertex2;
    GEO::index_t vertex2 = edge->vertex2;
    FacetCirculator circulator_start = incident_facets(vertex1, vertex2, edge->incidentFacet.first, edge->incidentFacet.second);
    FacetCirculator circulator = circulator_start;
    do {
        if(isFiniteCell(circulator.cell())) {
            LatticeVectorType trial_b = computeFacetBurgersVector(Facet(circulator.cell(), circulator.facet()));
            if(trial_b > 0 && trial_b != previous_b) {
                previous_b = trial_b;

                LatticeVectorType deltaLatticeVector = reverseLatticeVectors[trial_b];
                LatticeVectorType newLatticeVec = vectorSum(originalLatticeVector, deltaLatticeVector);
                if(newLatticeVec != -1) {
                    edge->latticeVector = newLatticeVec;
                    edge->oppositeEdge()->latticeVector = reverseLatticeVectors[newLatticeVec];

                    int improvement = 0;
                    int bestRecursiveImprovement = 0;
                    std::pair<Edge*,LatticeVectorType> bestRecursiveCutSurface[3];
                    bestRecursiveCutSurface[0].first = NULL;
                    FacetCirculator circulator2 = circulator_start;
                    do {
                        if(isFiniteCell(circulator2.cell())) {
                            LatticeVectorType b = computeFacetBurgersVector(Facet(circulator2.cell(), circulator2.facet()));
                            if(b != -1) {
                                if(b == deltaLatticeVector) improvement--;
                                else if(b == 0) improvement++;
                            }
                            else {
                                improvement = 0;
                                bestRecursiveImprovement = 0;
                                bestRecursiveCutSurface[0].first = NULL;
                                break;
                            }
                            if(depth != 0) {
                                Edge* edge1;
                                Edge* edge2;
                                int i = tessellation->index(circulator2.cell(), vertex1);
                                int j = tessellation->index(circulator2.cell(), vertex2);
                                computeAlternativeEdgeLatticeVector(circulator2.cell(), i, j, edge1, edge2);
                                std::pair<Edge*,LatticeVectorType> cutSurface[3];
                                int recursiveImprovement;

                                cutSurface[0].first = edge1;
                                recursiveImprovement = improveEdgeLatticeVectorRecursive(cutSurface, depth-1);
                                if(recursiveImprovement > bestRecursiveImprovement) {
                                    bestRecursiveImprovement = recursiveImprovement;
                                    std::copy(cutSurface, cutSurface + depth, bestRecursiveCutSurface);
                                }

                                cutSurface[0].first = edge2;
                                recursiveImprovement = improveEdgeLatticeVectorRecursive(cutSurface, depth-1);
                                if(recursiveImprovement > bestRecursiveImprovement) {
                                    bestRecursiveImprovement = recursiveImprovement;
                                    std::copy(cutSurface, cutSurface + depth, bestRecursiveCutSurface);
                                }
                            }
                        }
                        --circulator2;
                    }
                    while(circulator2 != circulator_start);
                    improvement += bestRecursiveImprovement;

                    // Restore original lattice vector.
                    edge->latticeVector = originalLatticeVector;
                    edge->oppositeEdge()->latticeVector = reverseLatticeVectors[originalLatticeVector];

                    if(improvement > bestImprovement) {
                        bestImprovement = improvement;
                        bestNewLatticeVector = newLatticeVec;
                        std::copy(bestRecursiveCutSurface, bestRecursiveCutSurface + depth, results + 1);
                    }
                }
            }
        }
        --circulator;
    }
    while(circulator != circulator_start);
    results[0].second = bestNewLatticeVector;
    return bestImprovement;
}

/*********************************************************************
 * Makes sure lattice vectors assigned to edges are consistent
 * across processors.
 *********************************************************************/
void DislocationIdentificationFix::synchronizeEdgeVectors(bool onlyChangedEdges)
{
    edgeTransmissionBuffer.resize(vertexEdges.size() * EDGE_RECORDS_PER_COMM_PASS);

    // This array is to keep track of which edge needs to be sent next for each vertex.
    std::vector<Edge*> currentVertexEdges(vertexEdges.begin(), vertexEdges.begin() + atom->nlocal);

    size_t numTransmittedLocalEdges = 0;

    // Perform repeated transmissions until all edges incident on all local atoms have been transmitted
    // and until all edges incident on ghost atoms have been received.
    for(int pass = 0; ; pass++) {

        // Pack lattice vectors of local edges into send buffer.
        // For each edge to be transmitted we send the unique tag of the second edge vertex and the assigned
        // lattice vector. The first first edge vertex is implicitly given by the atom the edge belongs to.
        bigint numTransmittedLocalEdgesCurrentPass = 0;
        std::vector<EdgeCommRecord>::iterator transmittedEdge = edgeTransmissionBuffer.begin();
        for(size_t atomIndex = 0; atomIndex < atom->nlocal; atomIndex++) {
            Edge* edge = currentVertexEdges[atomIndex];
            int nedges = 0;
            while(nedges < EDGE_RECORDS_PER_COMM_PASS && edge != NULL) {
                if(edge->isLocal() && (!onlyChangedEdges || edge->latticeVector != edge->initialLatticeVector)) {
                    transmittedEdge->tagVertex2 = atom->tag[edge->vertex2];
                    if(edge->isInitiallyFixed()) transmittedEdge->tagVertex2 = -transmittedEdge->tagVertex2;
                    transmittedEdge->lv = edge->latticeVector;
                    ++transmittedEdge;
                    numTransmittedLocalEdgesCurrentPass++;
                    nedges++;
                }
                edge = edge->nextLeavingEdge;
            }
            currentVertexEdges[atomIndex] = edge;
            // Fill remaining send buffer with zeros if there are no more edges to transmit.
            for(; nedges < EDGE_RECORDS_PER_COMM_PASS; nedges++) {
                transmittedEdge->tagVertex2 = 0;
                ++transmittedEdge;
            }
        }
        numTransmittedLocalEdges += numTransmittedLocalEdgesCurrentPass;

        bigint numTransmittedEdgesCurrentPass;
        MPI_Allreduce(&numTransmittedLocalEdgesCurrentPass, &numTransmittedEdgesCurrentPass, 1, MPI_LMP_BIGINT, MPI_SUM, lmp->world);
        if(debugOutput)
            printLog("Edge sync pass %i: transmitted " BIGINT_FORMAT " edges\n", pass + 1, numTransmittedEdgesCurrentPass);

        if(numTransmittedEdgesCurrentPass == 0)
            break;

        // Perform inter-processor communication.
        commStage = 2;
        comm->forward_comm_fix(this);

        // Write lattice vectors received from neighbor procs back to local tessellation edges.
        for(size_t atomIndex = atom->nlocal; atomIndex < vertexEdges.size(); atomIndex++) {
            transmittedEdge = edgeTransmissionBuffer.begin() + (atomIndex * EDGE_RECORDS_PER_COMM_PASS);
            for(int edgeIndex = 0; edgeIndex < EDGE_RECORDS_PER_COMM_PASS; edgeIndex++, ++transmittedEdge) {
                // End of partially filled transmission buffer is indicated by a zero entry:
                tagint vertex2tag = transmittedEdge->tagVertex2;
                if(vertex2tag == 0) break;
                bool isInitallyFixed = false;
                if(vertex2tag < 0) {
                    isInitallyFixed = true;
                    vertex2tag = -vertex2tag;
                }
                for(Edge* edge = vertexEdges[atomIndex]; edge != NULL; edge = edge->nextLeavingEdge) {
                    if(atom->tag[edge->vertex2] == vertex2tag) {
                        // Receiving edge should never be a local edge:
                        if(edge->isLocal())
                            error->one(FLERR,"Overwriting lattice vector of local edge\n");
                        edge->initialLatticeVector = edge->latticeVector = transmittedEdge->lv;
                        edge->oppositeEdge()->initialLatticeVector = edge->oppositeEdge()->latticeVector = reverseLatticeVectors[transmittedEdge->lv];
                        if(isInitallyFixed) {
                            edge->setIsInitiallyFixed();
                            edge->oppositeEdge()->setIsInitiallyFixed();
                        }
                        else {
                            edge->unsetIsInitiallyFixed();
                            edge->oppositeEdge()->unsetIsInitiallyFixed();
                        }
                    }
                }
            }
        }
    }

    // Reset lattice vectors of ghost edges that have changed locally but not remotely to initial values.
    if(onlyChangedEdges) {
        for(EdgeMemoryPool<>::iterator edge = edgePool.begin(); edge != edgePool.end(); ++edge) {
            if(!edge->isLocal()) {
                edge->latticeVector = edge->initialLatticeVector;
            }
        }
    }

    // Free working memory.
    std::vector<EdgeCommRecord>().swap(edgeTransmissionBuffer);

    if(debugOutput) {
        printLog("Number of transmitted local edges: %i\n", numTransmittedLocalEdges);
    }
}

/*********************************************************************
 * This is for MPI communication with neighbor nodes.
 *********************************************************************/
int DislocationIdentificationFix::pack_forward_comm(int n, int* list, double* buf, int pbc_flag, int* pbc)
{
    int m = 0;
    if(commStage == 0) {
        for(int i = 0; i < n; i++) {
            const Vector3& p = displacements[list[i]];
            buf[m++] = p[0];
            buf[m++] = p[1];
            buf[m++] = p[2];
        }
    }
    else if(commStage == 1) {
        double box[3][3] = {{0,0,0},{0,0,0},{0,0,0}};
        box[0][0] = domain->h[0] - reference_h[0];
        box[1][1] = domain->h[1] - reference_h[1];
        box[2][2] = domain->h[2] - reference_h[2];
        box[2][1] = domain->h[3] - reference_h[3];
        box[2][0] = domain->h[4] - reference_h[4];
        box[1][0] = domain->h[5] - reference_h[5];
        Vector3 shift(0,0,0);
        if(pbc_flag) {
            for(size_t i = 0; i < 3; i++)
                for(size_t j = 0; j < 3; j++)
                    shift[i] += pbc[j] * box[j][i];
        }
        for(int i = 0; i < n; i++) {
            const Vector3& p = displacements[list[i]];
            buf[m++] = p[0] + shift.x();
            buf[m++] = p[1] + shift.y();
            buf[m++] = p[2] + shift.z();
        }
    }
    else if(commStage == 2) {
        for(int i = 0; i < n; i++) {
            std::vector<EdgeCommRecord>::const_iterator transmittedEdge = edgeTransmissionBuffer.begin() + (list[i] * EDGE_RECORDS_PER_COMM_PASS);
            for(int j = 0; j < EDGE_RECORDS_PER_COMM_PASS; j++, ++transmittedEdge) {
                *reinterpret_cast<tagint*>(&buf[m++]) = transmittedEdge->tagVertex2;
                *reinterpret_cast<LatticeVectorType*>(&buf[m++]) = transmittedEdge->lv;
            }
        }
    }
    else if(commStage == 3) {
        for(int i = 0; i < n; i++) {
            int ii = list[i];
            buf[m++] = averagePositions[ii][0];
            buf[m++] = averagePositions[ii][1];
            buf[m++] = averagePositions[ii][2];
        }
    }
    return m;
}

/*********************************************************************
 * This is for MPI communication with neighbor nodes.
 *********************************************************************/
void DislocationIdentificationFix::unpack_forward_comm(int n, int first, double* buf)
{
    int last = first + n;
    if(commStage == 0 || commStage == 1) {
        for(int i = first; i < last; i++) {
            Vector3& p = displacements[i];
            p[0] = *buf++;
            p[1] = *buf++;
            p[2] = *buf++;
        }
    }
    else if(commStage == 2) {
        std::vector<EdgeCommRecord>::iterator transmittedEdge = edgeTransmissionBuffer.begin() + (first * EDGE_RECORDS_PER_COMM_PASS);
        for(int i = first; i < last; i++) {
            for(int j = 0; j < EDGE_RECORDS_PER_COMM_PASS; j++, ++transmittedEdge) {
                transmittedEdge->tagVertex2 = *reinterpret_cast<const tagint*>(buf++);
                transmittedEdge->lv = *reinterpret_cast<const LatticeVectorType*>(buf++);
            }
        }
    }
    else if(commStage == 3) {
        for(int i = first; i < last; i++) {
            averagePositions[i][0] = *buf++;
            averagePositions[i][1] = *buf++;
            averagePositions[i][2] = *buf++;
        }
    }
}

/*********************************************************************
 * Eliminates slip surface artifacts.
 *********************************************************************/
void DislocationIdentificationFix::eliminateSlipSurfaceArtifacts()
{
    if(debugOutput)
        printLog("Eliminating slip surface artifacts\n");


    // The maximum number of algorithm iterations to perform:
    const int maxIterations = 6;

    for(int iteration = 0; iteration < maxIterations; iteration++) {

        bigint nmodified = 0;
        std::vector< std::pair<LatticeVectorType, int> > votes;
        for(size_t i = 0; i < vertexEdges.size(); i++) {
            for(Edge* edge = vertexEdges[i]; edge != NULL; edge = edge->nextLeavingEdge) {
                if(edge->latticeVector == -1 || edge->deformedLatticeVector == -1) {
                    votes.clear();
                    break;
                }
                LatticeVectorType sv = vectorSum(edge->latticeVector, reverseLatticeVectors[edge->deformedLatticeVector]);
                if(sv == -1) {
                    votes.clear();
                    break;
                }
                std::vector< std::pair<LatticeVectorType, int> >::iterator iter = votes.begin();
                for(; iter != votes.end(); ++iter) {
                    if(iter->first == sv) {
                        iter->second++;
                        break;
                    }
                }
                if(iter == votes.end()) {
                    votes.push_back(std::make_pair(sv, 1));
                }
            }
            if(!votes.empty()) {
                std::vector< std::pair<LatticeVectorType, int> >::const_iterator max_vote = votes.begin();
                for(std::vector< std::pair<LatticeVectorType, int> >::const_iterator iter = max_vote + 1; iter != votes.end(); ++iter) {
                    if(iter->second > max_vote->second || (iter->second == max_vote->second && iter->first < max_vote->first)) {
                        max_vote = iter;
                    }
                }
                if(max_vote->first != 0) {
                    nmodified++;
                    for(Edge* edge = vertexEdges[i]; edge != NULL; edge = edge->nextLeavingEdge) {
                        LatticeVectorType new_lv = vectorSum(edge->latticeVector, reverseLatticeVectors[max_vote->first]);
                        if(new_lv != -1) {
                            edge->latticeVector = new_lv;
                            edge->oppositeEdge()->latticeVector = reverseLatticeVectors[new_lv];
                        }
                    }
                }
                votes.clear();
            }
        }

        bigint nmodified_total;
        MPI_Allreduce(&nmodified, &nmodified_total, 1, MPI_LMP_BIGINT, MPI_SUM, lmp->world);

        if(debugOutput)
            printLog(" Iteration %i: Modified " BIGINT_FORMAT " atoms\n", iteration+1, nmodified_total);

        if(nmodified_total == 0)
            break;

        // Transfer lattice vectors of edges between processors.
        synchronizeEdgeVectors(true);
    }
}

/*********************************************************************
 * Computes how much plastic slip ocurred on each slip system
 * and also the total plastic displacement gradient.
 *********************************************************************/
void DislocationIdentificationFix::computeSweptAreaPerSlipSystem()
{
    if(debugOutput)
        printLog("Computing swept area on %i slip systems\n", slipSystemSweptAreas.size());

    // Local plastic displacement gradient.
    double plastic_displ_gradient_local[3][3] = {{0,0,0},{0,0,0},{0,0,0}};

    // The projected area of plastic slip on the different slip systems.
    std::vector<double> slipSystemSweptAreasLocal(slipSystemSweptAreas.size(), 0.0);

    // The precomputed slip plane normals in the simulation coordinate system.
    std::vector<Vector3> transformedSlipPlaneNormals(slipSystems.size());
    for(size_t i = 0; i < slipSystems.size(); i++) {
        for(size_t a = 0; a < 3; a++) {
            transformedSlipPlaneNormals[i][a] = 0;
            for(size_t b = 0; b < 3; b++)
                transformedSlipPlaneNormals[i][a] += inv_lattice_rotation[a][b] * slipSystems[i].second[b];
        }
        // Normalize normal vector.
        transformedSlipPlaneNormals[i] /= transformedSlipPlaneNormals[i].length();
    }

    for(EdgeMemoryPool<>::iterator edge = edgePool.begin(); edge != edgePool.end(); ++edge, ++edge) {

        // Skip edges that did not slip.
        if(edge->latticeVector == edge->deformedLatticeVector)
            continue;

        // Skip edges without lattice vectors.
        if(edge->latticeVector == -1 || edge->deformedLatticeVector == -1)
            continue;

        // Make sure only one edge out of every pair is output.
        if(edge->isOddEdge()) continue;

        // Skip ghost edges.
        if(!edge->isLocal()) continue;

        // Compute slip vector.
        Vector3 slipVector = latticeVectors[edge->latticeVector] - latticeVectors[edge->deformedLatticeVector];
        if(reverseDislocationSense) slipVector = -slipVector;

        // Look up the slip system index for this slip vector.
        int slipSystemIndex = -1;
        Vector3 slipPlaneNormal;
        for(size_t i = 0; i < slipSystems.size(); i++) {
            if((slipSystems[i].first - slipVector).squared_length() < 1e-9) {
                slipSystemIndex = i;
                slipPlaneNormal = transformedSlipPlaneNormals[slipSystemIndex];
                break;
            }
            else if((slipSystems[i].first + slipVector).squared_length() < 1e-9) {
                slipSystemIndex = i;
                slipPlaneNormal = -transformedSlipPlaneNormals[slipSystemIndex];
                break;
            }
        }

        // Transform slip vector from lattice frame to simulation coordinate system.
        Vector3 transformedSlipVector(0,0,0);
        for(size_t a = 0; a < 3; a++) {
            for(size_t b = 0; b < 3; b++) {
                transformedSlipVector[a] += inv_lattice_rotation[a][b] * slipVector[b];
            }
        }

        // Loop over cells around the edge to count them.
        GEO::index_t vertex1 = edge->oppositeEdge()->vertex2;
        GEO::index_t vertex2 = edge->vertex2;
        FacetCirculator circulator_start = incident_facets(vertex1, vertex2, edge->incidentFacet.first, edge->incidentFacet.second);
        FacetCirculator circulator = circulator_start;
        int count = 0;
        Vector3 p0, v1;
        do {
            if(!isFiniteCell(circulator.cell()))
                error->one(FLERR, "Slip surface intersects infinite Delaunay cell. Ghost atom layer might not too small or there are free surfaces.");

            // Compute center coordinates of Delaunay cell.
            Vector3 com(0, 0, 0);
            for(int vindex = 0; vindex < 4; vindex++) {
                GEO::index_t atomIndex = tessellation->cell_vertex(circulator.cell(), vindex);
                com[0] += atom->x[atomIndex][0];
                com[1] += atom->x[atomIndex][1];
                com[2] += atom->x[atomIndex][2];
            }
            com[0] /= 4;
            com[1] /= 4;
            com[2] /= 4;
            if(count == 0) {
                p0 = com;
            }
            else if(count == 1) {
                v1 = com - p0;
            }
            else {
                // Compute projected area of triangle.
                Vector3 v2 = com - p0;
                Vector3 facetNormal = v2.cross(v1);
                facetNormal /= 2;
                // Compute contribution to one of the slip systems.
                if(slipSystemIndex != -1) {
                    double projectedArea = slipPlaneNormal.dot(facetNormal);
                    slipSystemSweptAreasLocal[slipSystemIndex] += projectedArea;
                }
                // Compute contribution to overall plastic displacement gradient.
                for(size_t i = 0; i < 3; i++) {
                    for(size_t j = 0; j < 3; j++) {
                        plastic_displ_gradient_local[i][j] += facetNormal[i] * transformedSlipVector[j];
                    }
                }
                v1 = v2;
            }

            ++circulator;
            ++count;
        }
        while(circulator != circulator_start);
    }

    // Compute aggregate slip system values over all procs.
    MPI_Allreduce(&slipSystemSweptAreasLocal.front(), &slipSystemSweptAreas.front(), slipSystemSweptAreas.size(), MPI_DOUBLE, MPI_SUM, lmp->world);

    // Compute aggregate plastic deformation.
    MPI_Allreduce(plastic_displ_gradient_local, plastic_displ_gradient, 3*3, MPI_DOUBLE, MPI_SUM, lmp->world);

    // Divide by simulation cell volume.
    double cell_vol = (domain->xprd * domain->yprd * domain->zprd);
    for(int i = 0; i < 3; i++)
        for(int j = 0; j < 3; j++)
            plastic_displ_gradient[i][j] /= cell_vol;
}

/*********************************************************************
 * Reports the memory usage of this fix to LAMMPS.
 *********************************************************************/
double DislocationIdentificationFix::memory_usage()
{
    double mem = edgePool.memoryUsage();
    if(performSlipAnalysis) {
        mem += sizeof(Vector3) * atom->nmax;
    }
    mem += sizeof(EdgeCommRecord) * (atom->nlocal + atom->nghost) * EDGE_RECORDS_PER_COMM_PASS;
    mem += sizeof(cellEdges.front()) * cellEdges.capacity();
    mem += sizeof(vertexEdges.front()) * (atom->nlocal + atom->nghost);
    return mem;
}

/*********************************************************************
 * Sends a formatted string to the log file and stdout.
 *********************************************************************/
void DislocationIdentificationFix::printLog(const char* format, ...)
{
    const static auto startTime = std::chrono::steady_clock::now();
    double timeStamp = std::chrono::duration<double>(std::chrono::steady_clock::now() - startTime).count();
    if(comm->me == 0 && screen) {
        va_list ap;
        va_start(ap,format);
        fprintf(screen, "FixDisloc (t=%.3f secs): ", timeStamp);
        vfprintf(screen, format, ap);
        va_end(ap);
    }
    if(comm->me == 0 && logfile) {
        va_list ap;
        va_start(ap,format);
        fprintf(logfile, "FixDisloc (t=%.3f secs): ", timeStamp);
        vfprintf(logfile, format, ap);
        va_end(ap);
    }
}

/*********************************************************************
 * Finds the circumcenter of a tetrahedron.
 *
 * Note: This code has been adopted from the tetcircumcenter() function found in the MeshKit code base.
 *********************************************************************/
static DislocationIdentificationFix::Vector3 tetrahedronCircumcenter(double a[3], double b[3], double c[3], double d[3])
{
    double xba, yba, zba, xca, yca, zca, xda, yda, zda;
    double balength, calength, dalength;
    double xcrosscd, ycrosscd, zcrosscd;
    double xcrossdb, ycrossdb, zcrossdb;
    double xcrossbc, ycrossbc, zcrossbc;
    double denominator;
    double xcirca, ycirca, zcirca;

    // Use coordinates relative to point A of the tetrahedron.
    xba = b[0] - a[0];
    yba = b[1] - a[1];
    zba = b[2] - a[2];
    xca = c[0] - a[0];
    yca = c[1] - a[1];
    zca = c[2] - a[2];
    xda = d[0] - a[0];
    yda = d[1] - a[1];
    zda = d[2] - a[2];

    // Squares of lengths of the edges incident to A.
    balength = xba * xba + yba * yba + zba * zba;
    calength = xca * xca + yca * yca + zca * zca;
    dalength = xda * xda + yda * yda + zda * zda;
    // Cross products of these edges.
    xcrosscd = yca * zda - yda * zca;
    ycrosscd = zca * xda - zda * xca;
    zcrosscd = xca * yda - xda * yca;
    xcrossdb = yda * zba - yba * zda;
    ycrossdb = zda * xba - zba * xda;
    zcrossdb = xda * yba - xba * yda;
    xcrossbc = yba * zca - yca * zba;
    ycrossbc = zba * xca - zca * xba;
    zcrossbc = xba * yca - xca * yba;

    // Calculate the denominator of the circumcenter formulae.
    denominator = 2.0 * (xba * xcrosscd + yba * ycrosscd + zba * zcrosscd);
    if(fabs(denominator) > 1e-8) {
        // Calculate vector (from A) to circumcenter.
        xcirca = (balength * xcrosscd + calength * xcrossdb + dalength * xcrossbc) / denominator;
        ycirca = (balength * ycrosscd + calength * ycrossdb + dalength * ycrossbc) / denominator;
        zcirca = (balength * zcrosscd + calength * zcrossdb + dalength * zcrossbc) / denominator;
    }
    else {
        // Special handling of degenerate tetrahedra. Fall back to the centroid in this case.
        xcirca = (xba + xca + xda) / 4;
        ycirca = (yba + yca + yda) / 4;
        zcirca = (zba + zca + zda) / 4;
    }

    return DislocationIdentificationFix::Vector3(xcirca + a[0], ycirca + a[1], zcirca + a[2]);
}

/*********************************************************************
 * Collects the dislocation and slip facet data to be written to the
 * output file.
 *********************************************************************/
void DislocationIdentificationFix::gatherOutputData(std::vector<GEO::index_t>& outputCells, std::vector<int>& outputCellIndices, std::vector<tagint>& dislocationSegments,
    std::vector<float>& segmentBurgersVectors, std::vector<float>& cellCenters, std::vector<tagint>& cellIds,
    std::vector<tagint>& slippedEdges, std::vector<float>& slipVectors, std::vector<float>& slipFacetNormals, std::vector<tagint>& slipSurfaceVertices, std::vector<int>& slipSurfaceEdgeCounts)
{
    if(debugOutput) {
        printLog("Gathering output data.\n");
    }
    outputCellIndices.resize(tessellation->nb_cells(), -1);

    for(GEO::index_t cell = 0; cell < tessellation->nb_cells(); ++cell) {

        // Skip infinite cells of the Delaunay tessellation.
        if(!isFiniteCell(cell))
            continue;

        // Determine if the cell is a local cell (i.e. belongs to the local processor).
        // Look for the vertex atom with the highest ID.
        // If it is a local atom, the cell is local.
        tagint highestAtomId = 0;
        bool isLocalCell;
        for(int v = 0; v < 4; v++) {
            GEO::index_t vertex = tessellation->cell_vertex(cell, v);
            tagint atomId = atom->tag[vertex];
            if(atomId > highestAtomId) {
                highestAtomId = atomId;
                isLocalCell = ((int)vertex < atom->nlocal);
            }
        }

        // Skip non-local cells which belong to neighboring processors.
        if(!isLocalCell)
            continue;

        // Gather lattice vectors assigned to the six edges of the tetrahedron.
        LatticeVectorType edgeVectors[6];
        for(size_t edgeIndex = 0; edgeIndex < 6; edgeIndex++) {
            edgeVectors[edgeIndex] = cellEdges[cell].edges[edgeIndex]->latticeVector;
//            edgeVectors[edgeIndex] = cellEdges[cell].edges[edgeIndex]->deformedLatticeVector;
        }

        // Iterate over the four faces of the tetrahedron cell.
        for(int f = 0; f < 4; f++) {

            // Perform Burgers circuit test on the facet.
            Vector3 b = latticeVectors[edgeVectors[burgersCircuitEdges[f][0]]] +
                        latticeVectors[edgeVectors[burgersCircuitEdges[f][1]]] -
                        latticeVectors[edgeVectors[burgersCircuitEdges[f][2]]];
            if(b.squared_length() < 1e-6)
                continue;
            // Correct sense for every other face.
            // Obey the dislocation sign convention selected by the user.
            if(((f & 1) != 0) != reverseDislocationSense) b = -b;

            Facet adjacentCell = mirrorFacet(cell, f);
            if(!isFiniteCell(adjacentCell.first))
                error->one(FLERR, "Dislocation line leads into an infinite tessellation cell. Possible reasons: Ghost layer thickness might be too small or there are free surfaces in the simulation.");

            // Make sure we emit every facet only once.
            if(atom->tag[tessellation->cell_vertex(cell, f)] > atom->tag[tessellation->cell_vertex(adjacentCell.first, adjacentCell.second)])
                continue;

            // Make sure the centers of the two cell adjacent to the facet get dumped.
            if(outputCellIndices[cell] == -1) {
                outputCellIndices[cell] = outputCells.size();
                outputCells.push_back(cell);
            }
            if(outputCellIndices[adjacentCell.first] == -1) {
                outputCellIndices[adjacentCell.first] = outputCells.size();
                outputCells.push_back(adjacentCell.first);
            }

            dislocationSegments.push_back(outputCellIndices[cell]);
            dislocationSegments.push_back(outputCellIndices[adjacentCell.first]);
            segmentBurgersVectors.push_back(b[0]);
            segmentBurgersVectors.push_back(b[1]);
            segmentBurgersVectors.push_back(b[2]);
        }
    }

    if(performSlipAnalysis) {
        for(EdgeMemoryPool<>::iterator edge = edgePool.begin(); edge != edgePool.end(); ++edge, ++edge) {

            // Skip edges that did not slip.
            if(edge->latticeVector == edge->deformedLatticeVector)
                continue;

            // Skip edges without lattice vectors.
            if(edge->latticeVector == -1 || edge->deformedLatticeVector == -1)
                continue;

            GEO::index_t vertex1 = edge->oppositeEdge()->vertex2;
            GEO::index_t vertex2 = edge->vertex2;
            tagint vertexTags[2];
            vertexTags[0] = atom->tag[vertex1];
            vertexTags[1] = atom->tag[vertex2];

            // Make sure only one edge out of every pair is output.
            if(edge->isOddEdge()) continue;

            // Skip ghost edges.
            if(!edge->isLocal()) continue;

            // Compute slip vector.
            Vector3 slipVector = latticeVectors[edge->latticeVector] - latticeVectors[edge->deformedLatticeVector];
            if(reverseDislocationSense) slipVector = -slipVector;
            slipVectors.push_back(slipVector[0]);
            slipVectors.push_back(slipVector[1]);
            slipVectors.push_back(slipVector[2]);

            // Compute crystallographic normal of the facet.
            Vector3 normalVector = latticeVectors[edge->latticeVector];
            slipFacetNormals.push_back(normalVector[0]);
            slipFacetNormals.push_back(normalVector[1]);
            slipFacetNormals.push_back(normalVector[2]);

            // Write out the two vertices of the edge.
            slippedEdges.push_back(vertexTags[0]);
            slippedEdges.push_back(vertexTags[1]);

            // Loop over cells around the edge to count them.
            FacetCirculator circulator_start = incident_facets(vertex1, vertex2, edge->incidentFacet.first, edge->incidentFacet.second);
            FacetCirculator circulator = circulator_start;
            int count = 0;
            do {
                if(!isFiniteCell(circulator.cell()))
                    error->one(FLERR, "Slip surface intersects infinite Delaunay cell. Ghost atom layer might not too small or there are free surfaces.");

                // Make sure this cell's center point will be emitted.
                if(outputCellIndices[circulator.cell()] == -1) {
                    outputCellIndices[circulator.cell()] = outputCells.size();
                    outputCells.push_back(circulator.cell());
                }

                slipSurfaceVertices.push_back(outputCellIndices[circulator.cell()]);
                count++;

                ++circulator;
            }
            while(circulator != circulator_start);
            slipSurfaceEdgeCounts.push_back(count);
        }
    }

#if 0
    if(extractStackingFaults) {
        stream << "stacking faults:\n";
        size_t numSFEdges = 0;
        for(EdgeMemoryPool<>::iterator edge = edgePool.begin(); edge != edgePool.end(); ++edge, ++edge) {

            // Skip edges with no lattice vector.
            if(edge->latticeVector == -1)
                continue;

            // Check if the edge's stacking fault displacement is a full lattice vector or not.
            LatticeVectorType sfvector = stackingFaultVectors[edge->latticeVector];
            if(sfvector == 0)
                continue;

            GEO::index_t vertex1 = edge->oppositeEdge()->vertex2;
            GEO::index_t vertex2 = edge->vertex2;
            tagint vertexTags[2];
            vertexTags[0] = atom->tag[vertex1];
            vertexTags[1] = atom->tag[vertex2];

            // Skip ghost edges.
            if(vertexTags[0] < vertexTags[1]) {
                if(vertex1 >= atom->nlocal) continue;
            }
            else {
                if(vertex2 >= atom->nlocal) continue;
            }

            // Output displacement vector.
            stream << sfvector;

            // Write out the two vertices of the edge.
            stream << std::hex;
            stream << " " << vertexTags[0] << " " << vertexTags[1];
            stream << std::dec;

            // Loop over cells around the edge to count them.
            FacetCirculator circulator_start = incident_facets(vertex1, vertex2, edge->incidentFacet.first, edge->incidentFacet.second);
            FacetCirculator circulator = circulator_start;
            int cellCount = 0;
            do {
                cellCount++;
                ++circulator;
            }
            while(circulator != circulator_start);
            stream << " " << cellCount;

            // Loop over cells around the edge to output them.
            stream << std::hex;
            do {
                if(!isFiniteCell(circulator.cell()))
                    error->one(FLERR,"Stacking fault intersects infinite Delaunay cell. Ghost atom layer might not too small or there are free surfaces.");

                // Make sure the cell center will be written out below.
                if(cellOutputIndices[circulator.cell()] == -1)
                    cellOutputIndices[circulator.cell()] = numOutputCells++;

                // Output the two cell vertices which do not form the SF edge.
                tagint nonEdgeVertexTags[2];
                int n = 0;
                for(int v = 0; v < 4; v++) {
                    GEO::index_t vert = tessellation->cell_vertex(circulator.cell(), v);
                    if(vert != vertex2 && vert != vertex1)
                        nonEdgeVertexTags[n++] = atom->tag[vert];
                }
                stream << " " << nonEdgeVertexTags[0] << " " << nonEdgeVertexTags[1];

                ++circulator;
            }
            while(circulator != circulator_start);

            stream << std::dec;
            stream << "\n";

            numSFEdges++;
        }
        stream << "end of stacking faults:\n";
        if(debugOutput)
            printLog("Number of stacking fault edges: %i\n", numSFEdges);
    }
#endif

    // Generate list with all Delaunay cell center coordinates and their unique node IDs.
    cellCenters.resize(outputCells.size() * 3);
    cellIds.resize(outputCells.size() * 4);
    std::vector<float>::iterator center = cellCenters.begin();
    std::vector<tagint>::iterator id = cellIds.begin();
    for(std::vector<GEO::index_t>::const_iterator cell = outputCells.begin(); cell != outputCells.end(); ++cell) {
#if 0
        // Use centroid of tetrahedron as nodal location.
        double com[3] = {0,0,0};
        tagint vertexTags[4];
        for(int vindex = 0; vindex < 4; vindex++) {
            GEO::index_t atomIndex = tessellation->cell_vertex(*cell, vindex);
            vertexTags[vindex] = atom->tag[atomIndex];
            com[0] += atom->x[atomIndex][0];
            com[1] += atom->x[atomIndex][1];
            com[2] += atom->x[atomIndex][2];
        }
        *center++ = (float)(com[0] / 4);
        *center++ = (float)(com[1] / 4);
        *center++ = (float)(com[2] / 4);
#else
        // Use circumcenter of tetrahedron as the nodal location (Voronoi facet vertices).
        GEO::index_t atomIndices[4];
        tagint vertexTags[4];
        for(int vindex = 0; vindex < 4; vindex++) {
            atomIndices[vindex] = tessellation->cell_vertex(*cell, vindex);
            vertexTags[vindex] = atom->tag[atomIndices[vindex]];
        }
        Vector3 cc = tetrahedronCircumcenter(atom->x[atomIndices[0]], atom->x[atomIndices[1]], atom->x[atomIndices[2]], atom->x[atomIndices[3]]);
        *center++ = cc[0];
        *center++ = cc[1];
        *center++ = cc[2];
#endif
        // Give node ID, which is derived from the tags of the four vertex atoms, a unique ordering.
        std::sort(vertexTags, vertexTags + 4);
        for(int i = 0; i < 4; i++)
            *id++ = vertexTags[i];
    }
}

/*********************************************************************
 * Writes the dislocation segments to the NetCDF output file.
 *********************************************************************/
void DislocationIdentificationFix::writeOutputFile(const std::string& filename)
{
    // Extract the dislocation segments and slip surface facets.
    std::vector<GEO::index_t> outputCells;
    std::vector<int> outputCellIndices;
    std::vector<tagint> dislocationSegments;
    std::vector<float> segmentBurgersVectors;
    std::vector<float> cellCenters;
    std::vector<tagint> cellIds;
    std::vector<tagint> slippedEdges;
    std::vector<float> slipVectors;
    std::vector<float> slipFacetNormals;
    std::vector<tagint> slipSurfaceVertices;
    std::vector<int> slipSurfaceEdgeCounts;
    gatherOutputData(outputCells, outputCellIndices, dislocationSegments, segmentBurgersVectors, cellCenters, cellIds, slippedEdges, slipVectors, slipFacetNormals, slipSurfaceVertices, slipSurfaceEdgeCounts);

    // Determine the total number of output elements across all processors.
    struct OutputElementCounts {
        tagint numDislocationSegments;
        tagint numSlipFacets;
        tagint numSlipFacetVertices;
        tagint numOutputCells;
    };
    OutputElementCounts localCounts, totalCounts, cummulativeCounts;
    localCounts.numDislocationSegments = segmentBurgersVectors.size() / 3;
    localCounts.numSlipFacets = slipSurfaceEdgeCounts.size();
    localCounts.numSlipFacetVertices = slipSurfaceVertices.size();
    localCounts.numOutputCells = outputCells.size();
#if !FIX_DISLOC_USE_PARALLEL_IO
    MPI_Reduce(&localCounts, &totalCounts, 4, MPI_LMP_TAGINT, MPI_SUM, 0, lmp->world);
#else
    MPI_Allreduce(&localCounts, &totalCounts, 4, MPI_LMP_TAGINT, MPI_SUM, lmp->world);
#endif
#ifndef MPI_STUBS
    MPI_Exscan(&localCounts, &cummulativeCounts, 4, MPI_LMP_TAGINT, MPI_SUM, lmp->world);
#endif
    if(debugOutput && comm->me == 0) {
        printLog("Number of output nodes: " TAGINT_FORMAT "\n", totalCounts.numOutputCells);
        printLog("Number of output dislocation segments: " TAGINT_FORMAT "\n", totalCounts.numDislocationSegments);
        if(performSlipAnalysis)
            printLog("Number of output slip facets: " TAGINT_FORMAT "\n", totalCounts.numSlipFacets);
    }

    // Offset indexes into the global cells list.
    if(comm->me != 0) {
        for(std::vector<tagint>::iterator iter = dislocationSegments.begin(); iter != dislocationSegments.end(); ++iter)
            *iter += cummulativeCounts.numOutputCells;
        for(std::vector<tagint>::iterator iter = slipSurfaceVertices.begin(); iter != slipSurfaceVertices.end(); ++iter)
            *iter += cummulativeCounts.numOutputCells;
    }

    // Aggregate distributed data arrays on a smaller number of processors to speed up file I/O.
#if !FIX_DISLOC_USE_PARALLEL_IO
    if(aggregateIOLevel > 0) {
        for(int level = 1; level <= aggregateIOLevel; level++) {
            if(debugOutput) {
                printLog("Aggregating output data (iteration %i of %i)\n", level, aggregateIOLevel);
            }
            if((comm->me % (1<<level)) == (1<<(level-1))) {
                // Send data during this iteration.
                int destRank = (comm->me / (1<<level)) * (1<<level);

                // Send element counts.
                MPI_Send(&localCounts, 4, MPI_LMP_TAGINT, destRank, 0, lmp->world);

                // Send data arrays.
                if(localCounts.numOutputCells != 0) {
                    MPI_Send(&cellCenters[0], cellCenters.size(), MPI_FLOAT, destRank, 0, lmp->world);
                    MPI_Send(&cellIds[0], cellIds.size(), MPI_LMP_TAGINT, destRank, 0, lmp->world);
                }
                if(localCounts.numDislocationSegments != 0) {
                    MPI_Send(&segmentBurgersVectors[0], segmentBurgersVectors.size(), MPI_FLOAT, destRank, 0, lmp->world);
                    MPI_Send(&dislocationSegments[0], dislocationSegments.size(), MPI_LMP_TAGINT, destRank, 0, lmp->world);
                }
                if(localCounts.numSlipFacets != 0) {
                    MPI_Send(&slipVectors[0], slipVectors.size(), MPI_FLOAT, destRank, 0, lmp->world);
                    MPI_Send(&slipFacetNormals[0], slipFacetNormals.size(), MPI_FLOAT, destRank, 0, lmp->world);
                    MPI_Send(&slippedEdges[0], slippedEdges.size(), MPI_LMP_TAGINT, destRank, 0, lmp->world);
                    MPI_Send(&slipSurfaceEdgeCounts[0], slipSurfaceEdgeCounts.size(), MPI_INT, destRank, 0, lmp->world);
                }
                if(localCounts.numSlipFacetVertices != 0) {
                    MPI_Send(&slipSurfaceVertices[0], slipSurfaceVertices.size(), MPI_LMP_TAGINT, destRank, 0, lmp->world);
                }
            }
            else if((comm->me % (1<<level)) == 0) {
                // Receive data during this iteration.
                int sourceRank = comm->me + (1<<(level-1));
                if(sourceRank >= comm->nprocs) continue;
                MPI_Status status;

                // Receive element counts.
                OutputElementCounts remoteCounts;
                MPI_Recv(&remoteCounts, 4, MPI_LMP_TAGINT, sourceRank, 0, lmp->world, &status);

                // Resize memory buffers to accommodate data to be received.
                cellCenters.resize(cellCenters.size() + remoteCounts.numOutputCells * 3);
                cellIds.resize(cellIds.size() + remoteCounts.numOutputCells * 4);
                segmentBurgersVectors.resize(segmentBurgersVectors.size() + remoteCounts.numDislocationSegments * 3);
                dislocationSegments.resize(dislocationSegments.size() + remoteCounts.numDislocationSegments * 2);
                slipVectors.resize(slipVectors.size() + remoteCounts.numSlipFacets * 3);
                slipFacetNormals.resize(slipFacetNormals.size() + remoteCounts.numSlipFacets * 3);
                slippedEdges.resize(slippedEdges.size() + remoteCounts.numSlipFacets * 2);
                slipSurfaceEdgeCounts.resize(slipSurfaceEdgeCounts.size() + remoteCounts.numSlipFacets);
                slipSurfaceVertices.resize(slipSurfaceVertices.size() + remoteCounts.numSlipFacetVertices);

                // Receive data arrays.
                if(remoteCounts.numOutputCells != 0) {
                    MPI_Recv(&cellCenters[localCounts.numOutputCells * 3], remoteCounts.numOutputCells * 3, MPI_FLOAT, sourceRank, 0, lmp->world, &status);
                    MPI_Recv(&cellIds[localCounts.numOutputCells * 4], remoteCounts.numOutputCells * 4, MPI_LMP_TAGINT, sourceRank, 0, lmp->world, &status);
                    localCounts.numOutputCells += remoteCounts.numOutputCells;
                }
                if(remoteCounts.numDislocationSegments != 0) {
                    MPI_Recv(&segmentBurgersVectors[localCounts.numDislocationSegments * 3], remoteCounts.numDislocationSegments * 3, MPI_FLOAT, sourceRank, 0, lmp->world, &status);
                    MPI_Recv(&dislocationSegments[localCounts.numDislocationSegments * 2], remoteCounts.numDislocationSegments * 2, MPI_LMP_TAGINT, sourceRank, 0, lmp->world, &status);
                    localCounts.numDislocationSegments += remoteCounts.numDislocationSegments;
                }
                if(remoteCounts.numSlipFacets != 0) {
                    MPI_Recv(&slipVectors[localCounts.numSlipFacets * 3], remoteCounts.numSlipFacets * 3, MPI_FLOAT, sourceRank, 0, lmp->world, &status);
                    MPI_Recv(&slipFacetNormals[localCounts.numSlipFacets * 3], remoteCounts.numSlipFacets * 3, MPI_FLOAT, sourceRank, 0, lmp->world, &status);
                    MPI_Recv(&slippedEdges[localCounts.numSlipFacets * 2], remoteCounts.numSlipFacets * 2, MPI_LMP_TAGINT, sourceRank, 0, lmp->world, &status);
                    MPI_Recv(&slipSurfaceEdgeCounts[localCounts.numSlipFacets], remoteCounts.numSlipFacets, MPI_INT, sourceRank, 0, lmp->world, &status);
                    localCounts.numSlipFacets += remoteCounts.numSlipFacets;
                }
                if(remoteCounts.numSlipFacetVertices != 0) {
                    MPI_Recv(&slipSurfaceVertices[localCounts.numSlipFacetVertices], remoteCounts.numSlipFacetVertices, MPI_LMP_TAGINT, sourceRank, 0, lmp->world, &status);
                    localCounts.numSlipFacetVertices += remoteCounts.numSlipFacetVertices;
                }
            }
            else {
                // Do nothing during this iteration.
            }
        }
    }
#endif

    int ncid;

    // Open output file for writing.
#if FIX_DISLOC_USE_PARALLEL_IO
    if(nc_create_par(filename.c_str(), NC_NETCDF4 | NC_MPIIO, lmp->world, MPI_INFO_NULL, &ncid) != NC_NOERR)
        error->one(FLERR, "Failed to open disloc output file for writing (parallel mode).");
#else
    if(comm->me == 0) {
	#ifdef LMP_NC_CLASSIC
        if(nc_create(filename.c_str(), NC_CLASSIC, &ncid) != NC_NOERR)
	#else
        if(nc_create(filename.c_str(), NC_NETCDF4, &ncid) != NC_NOERR)
	#endif
            error->one(FLERR, "Failed to open disloc output file for writing.");
#endif

    // Global attributes.
    nc_put_att_text(ncid, NC_GLOBAL, "Conventions", 9, "FixDisloc");
    nc_put_att_text(ncid, NC_GLOBAL, "ConventionVersion", strlen(FIX_DISLOC_FORMAT_VERSION), FIX_DISLOC_FORMAT_VERSION);
    long long timestep = update->ntimestep;
    nc_put_att_longlong(ncid, NC_GLOBAL, "TimestepNumber", NC_INT64, 1, &timestep);
    nc_put_att_double(ncid, NC_GLOBAL, "TimestepSize", NC_DOUBLE, 1, &update->dt);
    double timeInterval = this->nevery * update->dt;
    nc_put_att_double(ncid, NC_GLOBAL, "TimeInterval", NC_DOUBLE, 1, &timeInterval);
    nc_put_att_text(ncid, NC_GLOBAL, "LatticeStructure", latticeStructure.size(), latticeStructure.c_str());

    // Define NetCDF dimensions.
    int spatial_dim, nodes_dim, dislocation_segments_dim, slip_facets_dim, slip_facet_vertices_dim, pair_dim, node_id_dim;
    nc_def_dim(ncid, "spatial", 3, &spatial_dim);
    nc_def_dim(ncid, "nodes", totalCounts.numOutputCells, &nodes_dim);
    nc_def_dim(ncid, "dislocations", totalCounts.numDislocationSegments, &dislocation_segments_dim);
    if(performSlipAnalysis) {
        nc_def_dim(ncid, "slip_facets", totalCounts.numSlipFacets, &slip_facets_dim);
        nc_def_dim(ncid, "slip_facet_vertices", totalCounts.numSlipFacetVertices, &slip_facet_vertices_dim);
    }
    nc_def_dim(ncid, "pair", 2, &pair_dim);
    nc_def_dim(ncid, "node_id", 4, &node_id_dim);

    // Define NetCDF variables.
    int cell_vectors_var, cell_origin_var, cell_pbc_var, lattice_orientation_var;
    int dims[3] = { spatial_dim, spatial_dim, 0 };
    nc_def_var(ncid, "cell_vectors", NC_DOUBLE, 2, dims, &cell_vectors_var);
    nc_def_var(ncid, "cell_origin", NC_DOUBLE, 1, &spatial_dim, &cell_origin_var);
    nc_def_var(ncid, "cell_pbc", NC_INT, 1, &spatial_dim, &cell_pbc_var);
    nc_def_var(ncid, "lattice_orientation", NC_DOUBLE, 2, dims, &lattice_orientation_var);
    int nodal_ids_var;
    dims[0] = nodes_dim;
    dims[1] = node_id_dim;
    nc_def_var(ncid, "nodal_ids", NC_TAGINT_TYPE, 2, dims, &nodal_ids_var);
    int nodal_positions_var;
    dims[0] = nodes_dim;
    dims[1] = spatial_dim;
    nc_def_var(ncid, "nodal_positions", NC_FLOAT, 2, dims, &nodal_positions_var);
    int burgers_vectors_var;
    dims[0] = dislocation_segments_dim;
    dims[1] = spatial_dim;
    nc_def_var(ncid, "burgers_vectors", NC_FLOAT, 2, dims, &burgers_vectors_var);
    int dislocation_segments_var;
    dims[0] = dislocation_segments_dim;
    dims[1] = pair_dim;
    nc_def_var(ncid, "dislocation_segments", NC_TAGINT_TYPE, 2, dims, &dislocation_segments_var);
    int slipped_edges_var;
    int slip_vectors_var;
    int slip_facet_normals_var;
    int slip_facet_edge_counts_var;
    int slip_facet_vertices_var;
    if(performSlipAnalysis) {
        dims[0] = slip_facets_dim;
        dims[1] = pair_dim;
        nc_def_var(ncid, "slipped_edges", NC_TAGINT_TYPE, 2, dims, &slipped_edges_var);
        dims[0] = slip_facets_dim;
        dims[1] = spatial_dim;
        nc_def_var(ncid, "slip_vectors", NC_FLOAT, 2, dims, &slip_vectors_var);
        nc_def_var(ncid, "slip_facet_normals", NC_FLOAT, 2, dims, &slip_facet_normals_var);
        nc_def_var(ncid, "slip_facet_edge_counts", NC_INT, 1, &slip_facets_dim, &slip_facet_edge_counts_var);
        nc_def_var(ncid, "slip_facet_vertices", NC_TAGINT_TYPE, 1, &slip_facet_vertices_dim, &slip_facet_vertices_var);
    }

    // Done with definitions.
    nc_enddef(ncid);

    // Store simulation cell geometry and boundary conditions.
    const double cell_vectors[3][3] = {
        { domain->h[0], 0, 0 },
        { domain->h[5], domain->h[1], 0 },
        { domain->h[4], domain->h[3], domain->h[2] }
    };
    const double cell_origin[3] = { domain->boxlo[0], domain->boxlo[1], domain->boxlo[2] };
    const int pbc[3] = { domain->periodicity[0], domain->periodicity[1], domain->periodicity[2] };
#if FIX_DISLOC_USE_PARALLEL_IO
    nc_var_par_access(ncid, cell_vectors_var, NC_INDEPENDENT);
    nc_var_par_access(ncid, cell_origin_var, NC_INDEPENDENT);
    nc_var_par_access(ncid, cell_pbc_var, NC_INDEPENDENT);
    nc_var_par_access(ncid, lattice_orientation_var, NC_INDEPENDENT);
    if(comm->me == 0) {
#endif
        nc_put_var_double(ncid, cell_vectors_var, &cell_vectors[0][0]);
        nc_put_var_double(ncid, cell_origin_var, cell_origin);
        nc_put_var_int(ncid, cell_pbc_var, pbc);
        nc_put_var_double(ncid, lattice_orientation_var, &inv_lattice_rotation[0][0]);
#if FIX_DISLOC_USE_PARALLEL_IO
    }
#endif

    // Write payload data.
    size_t nodal_positions_start[] = { 0, 0 };
    size_t nodal_positions_count[] = { cellCenters.size() / 3, 3 };
    size_t nodal_ids_start[] = { 0, 0 };
    size_t nodal_ids_count[] = { cellIds.size() / 4, 4 };
    size_t burgers_vectors_start[] = { 0, 0 };
    size_t burgers_vectors_count[] = { segmentBurgersVectors.size() / 3, 3 };
    size_t dislocation_segments_start[] = { 0, 0 };
    size_t dislocation_segments_count[] = { dislocationSegments.size() / 2, 2 };
    size_t slipped_edges_start[] = { 0, 0 };
    size_t slipped_edges_count[] = { slippedEdges.size() / 2, 2 };
    size_t slip_vectors_start[] = { 0, 0 };
    size_t slip_vectors_count[] = { slipVectors.size() / 3, 3 };
    size_t slip_facet_normals_start[] = { 0, 0 };
    size_t slip_facet_normals_count[] = { slipFacetNormals.size() / 3, 3 };
    size_t slip_facet_vertices_start[] = { 0 };
    size_t slip_facet_vertices_count[] = { slipSurfaceVertices.size() };
    size_t slip_facet_edge_counts_start[] = { 0 };
    size_t slip_facet_edge_counts_count[] = { slipSurfaceEdgeCounts.size() };

#if !FIX_DISLOC_USE_PARALLEL_IO
    if(debugOutput) {
        printLog("Writing payload data from %i processors to NetCDF output file.\n", std::max(1, comm->nprocs / (1<<aggregateIOLevel)));
    }
    for(int proc = 0; proc < comm->nprocs; proc++) {
        if(aggregateIOLevel > 0 && (proc % (1<<aggregateIOLevel)) != 0)
            continue;

        // Receive data chunks from the other processors.
        if(proc != 0) {
            MPI_Status status;
            int sourceRank = proc;

            // Receive element counts.
            OutputElementCounts remoteCounts;
            MPI_Recv(&remoteCounts, 4, MPI_LMP_TAGINT, sourceRank, 0, lmp->world, &status);

            // Resize memory buffers to accommodate data to be received.
            cellCenters.resize(remoteCounts.numOutputCells * 3);
            cellIds.resize(remoteCounts.numOutputCells * 4);
            segmentBurgersVectors.resize(remoteCounts.numDislocationSegments * 3);
            dislocationSegments.resize(remoteCounts.numDislocationSegments * 2);
            slipVectors.resize(remoteCounts.numSlipFacets * 3);
            slipFacetNormals.resize(remoteCounts.numSlipFacets * 3);
            slippedEdges.resize(remoteCounts.numSlipFacets * 2);
            slipSurfaceEdgeCounts.resize(remoteCounts.numSlipFacets);
            slipSurfaceVertices.resize(remoteCounts.numSlipFacetVertices);

            // Receive data arrays.
            if(remoteCounts.numOutputCells != 0) {
                MPI_Recv(&cellCenters[0], remoteCounts.numOutputCells * 3, MPI_FLOAT, sourceRank, 0, lmp->world, &status);
                MPI_Recv(&cellIds[0], remoteCounts.numOutputCells * 4, MPI_LMP_TAGINT, sourceRank, 0, lmp->world, &status);
            }
            if(remoteCounts.numDislocationSegments != 0) {
                MPI_Recv(&segmentBurgersVectors[0], remoteCounts.numDislocationSegments * 3, MPI_FLOAT, sourceRank, 0, lmp->world, &status);
                MPI_Recv(&dislocationSegments[0], remoteCounts.numDislocationSegments * 2, MPI_LMP_TAGINT, sourceRank, 0, lmp->world, &status);
            }
            if(remoteCounts.numSlipFacets != 0) {
                MPI_Recv(&slipVectors[0], remoteCounts.numSlipFacets * 3, MPI_FLOAT, sourceRank, 0, lmp->world, &status);
                MPI_Recv(&slipFacetNormals[0], remoteCounts.numSlipFacets * 3, MPI_FLOAT, sourceRank, 0, lmp->world, &status);
                MPI_Recv(&slippedEdges[0], remoteCounts.numSlipFacets * 2, MPI_LMP_TAGINT, sourceRank, 0, lmp->world, &status);
                MPI_Recv(&slipSurfaceEdgeCounts[0], remoteCounts.numSlipFacets, MPI_INT, sourceRank, 0, lmp->world, &status);
            }
            if(remoteCounts.numSlipFacetVertices != 0) {
                MPI_Recv(&slipSurfaceVertices[0], remoteCounts.numSlipFacetVertices, MPI_LMP_TAGINT, sourceRank, 0, lmp->world, &status);
            }
            localCounts = remoteCounts;

            nodal_positions_count[0] = localCounts.numOutputCells;
            nodal_ids_count[0] = localCounts.numOutputCells;
            burgers_vectors_count[0] = localCounts.numDislocationSegments;
            dislocation_segments_count[0] = localCounts.numDislocationSegments;
            slip_vectors_count[0] = localCounts.numSlipFacets;
            slip_facet_normals_count[0] = localCounts.numSlipFacets;
            slipped_edges_count[0] = localCounts.numSlipFacets;
            slip_facet_edge_counts_count[0] = localCounts.numSlipFacets;
            slip_facet_vertices_count[0] = localCounts.numSlipFacetVertices;
        }
#else
        if(debugOutput) printLog("Setting collective variable access\n");
        nc_var_par_access(ncid, nodal_ids_var, NC_COLLECTIVE);
        nc_var_par_access(ncid, nodal_positions_var, NC_COLLECTIVE);
        nc_var_par_access(ncid, burgers_vectors_var, NC_COLLECTIVE);
        nc_var_par_access(ncid, dislocation_segments_var, NC_COLLECTIVE);
        if(performSlipAnalysis) {
            nc_var_par_access(ncid, slipped_edges_var, NC_COLLECTIVE);
            nc_var_par_access(ncid, slip_vectors_var, NC_COLLECTIVE);
            nc_var_par_access(ncid, slip_facet_normals_var, NC_COLLECTIVE);
            nc_var_par_access(ncid, slip_facet_vertices_var, NC_COLLECTIVE);
            nc_var_par_access(ncid, slip_facet_edge_counts_var, NC_COLLECTIVE);
        }

        if(comm->me != 0) {
            nodal_ids_start[0] = cummulativeNumOutputCells;
            nodal_positions_start[0] = cummulativeNumOutputCells;
            burgers_vectors_start[0] = cummulativeNumDislocationSegments;
            dislocation_segments_start[0] = cummulativeNumDislocationSegments;
            slipped_edges_start[0] = cummulativeNumSlipFacets;
            slip_vectors_start[0] = cummulativeNumSlipFacets;
            slip_facet_normals_start[0] = cummulativeNumSlipFacets;
            slip_facet_vertices_start[0] = cummulativeNumSlipFacetVertices;
            slip_facet_edge_counts_start[0] = cummulativeNumSlipFacets;
        }

        if(debugOutput) printLog("Putting netcdf variable data\n");
#endif
        // Write per-processor data chunks.
        nc_put_vara_tagint(ncid, nodal_ids_var, nodal_ids_start, nodal_ids_count, cellIds.empty() ? NULL : &cellIds[0]);
        nc_put_vara_float(ncid, nodal_positions_var, nodal_positions_start, nodal_positions_count, cellCenters.empty() ? NULL : &cellCenters[0]);
        nc_put_vara_float(ncid, burgers_vectors_var, burgers_vectors_start, burgers_vectors_count, segmentBurgersVectors.empty() ? NULL : &segmentBurgersVectors[0]);
        nc_put_vara_tagint(ncid, dislocation_segments_var, dislocation_segments_start, dislocation_segments_count, dislocationSegments.empty() ? NULL : &dislocationSegments[0]);
        if(performSlipAnalysis) {
            nc_put_vara_tagint(ncid, slipped_edges_var, slipped_edges_start, slipped_edges_count, slippedEdges.empty() ? NULL : &slippedEdges[0]);
            nc_put_vara_float(ncid, slip_vectors_var, slip_vectors_start, slip_vectors_count, slipVectors.empty() ? NULL : &slipVectors[0]);
            nc_put_vara_float(ncid, slip_facet_normals_var, slip_facet_normals_start, slip_facet_normals_count, slipFacetNormals.empty() ? NULL : &slipFacetNormals[0]);
            nc_put_vara_tagint(ncid, slip_facet_vertices_var, slip_facet_vertices_start, slip_facet_vertices_count, slipSurfaceVertices.empty() ? NULL : &slipSurfaceVertices[0]);
            nc_put_vara_int(ncid, slip_facet_edge_counts_var, slip_facet_edge_counts_start, slip_facet_edge_counts_count, slipSurfaceEdgeCounts.empty() ? NULL : &slipSurfaceEdgeCounts[0]);
        }

#if FIX_DISLOC_USE_PARALLEL_IO
        if(debugOutput) printLog("Done putting netcdf variable data\n");
#else
        nodal_ids_start[0] += nodal_ids_count[0];
        nodal_positions_start[0] += nodal_positions_count[0];
        burgers_vectors_start[0] += burgers_vectors_count[0];
        dislocation_segments_start[0] += dislocation_segments_count[0];
        slipped_edges_start[0] += slipped_edges_count[0];
        slip_vectors_start[0] += slip_vectors_count[0];
        slip_facet_normals_start[0] += slip_facet_normals_count[0];
        slip_facet_vertices_start[0] += slip_facet_vertices_count[0];
        slip_facet_edge_counts_start[0] += slip_facet_edge_counts_count[0];
    }
    }
    else if(aggregateIOLevel <= 0 || (comm->me % (1<<aggregateIOLevel)) == 0) {
        // Transmit output data from this processor to the master, which will write the data to the output file.
        int destRank = 0;

        // Send element counts.
        MPI_Send(&localCounts, 4, MPI_LMP_TAGINT, destRank, 0, lmp->world);

        // Send data arrays.
        if(localCounts.numOutputCells != 0) {
            MPI_Send(&cellCenters[0], cellCenters.size(), MPI_FLOAT, destRank, 0, lmp->world);
            MPI_Send(&cellIds[0], cellIds.size(), MPI_LMP_TAGINT, destRank, 0, lmp->world);
        }
        if(localCounts.numDislocationSegments != 0) {
            MPI_Send(&segmentBurgersVectors[0], segmentBurgersVectors.size(), MPI_FLOAT, destRank, 0, lmp->world);
            MPI_Send(&dislocationSegments[0], dislocationSegments.size(), MPI_LMP_TAGINT, destRank, 0, lmp->world);
        }
        if(localCounts.numSlipFacets != 0) {
            MPI_Send(&slipVectors[0], slipVectors.size(), MPI_FLOAT, destRank, 0, lmp->world);
            MPI_Send(&slipFacetNormals[0], slipFacetNormals.size(), MPI_FLOAT, destRank, 0, lmp->world);
            MPI_Send(&slippedEdges[0], slippedEdges.size(), MPI_LMP_TAGINT, destRank, 0, lmp->world);
            MPI_Send(&slipSurfaceEdgeCounts[0], slipSurfaceEdgeCounts.size(), MPI_INT, destRank, 0, lmp->world);
        }
        if(localCounts.numSlipFacetVertices != 0) {
            MPI_Send(&slipSurfaceVertices[0], slipSurfaceVertices.size(), MPI_LMP_TAGINT, destRank, 0, lmp->world);
        }
    }
#endif

#if !FIX_DISLOC_USE_PARALLEL_IO
    if(comm->me == 0) {
#endif
        if(debugOutput) printLog("Closing NetCDF file\n");

        // Close the netcdf file.
        if(nc_close(ncid) != NC_NOERR)
            error->one(FLERR,"Failed to close disloc output file after writing to it.");

#if !FIX_DISLOC_USE_PARALLEL_IO
    }
#endif
}

/******************************************************************************
* Is called by the system to request the global vector computed by the fix.
******************************************************************************/
double DislocationIdentificationFix::compute_vector(int n)
{
    if(n < 9)
        return plastic_displ_gradient[n/3][n%3];
    n -= 9;
    if(n < slipSystemSweptAreas.size())
        return slipSystemSweptAreas[n];
    return 0.0;
}

/**************************************************************************************
 * Writes a VTK file containing all triangle facets intersected by a dislocation.
 * This function is only for debugging purposes.
 **************************************************************************************/
void DislocationIdentificationFix::dumpDislocatedFacets(const std::string& filename)
{
    std::vector<int> dislocFaces;
    for(GEO::index_t cell = 0; cell < tessellation->nb_cells(); ++cell) {

        // Skip infinite cells.
        if(!isFiniteCell(cell))
            continue;

        // Gather lattice vectors assigned to the six edges of the tetrahedron.
        LatticeVectorType edgeVectors[6];
        for(size_t edgeIndex = 0; edgeIndex < 6; edgeIndex++)
            edgeVectors[edgeIndex] = cellEdges[cell].edges[edgeIndex]->latticeVector;

        // Perform Burgers test on three of the four tetrahedron facets.
        for(int face = 0; face < 4; face++) {
            if(edgeVectors[burgersCircuitEdges[face][0]] == -1) continue;
            if(edgeVectors[burgersCircuitEdges[face][1]] == -1) continue;
            if(edgeVectors[burgersCircuitEdges[face][2]] == -1) continue;
            Vector3 b = latticeVectors[edgeVectors[burgersCircuitEdges[face][0]]] +
                        latticeVectors[edgeVectors[burgersCircuitEdges[face][1]]] -
                        latticeVectors[edgeVectors[burgersCircuitEdges[face][2]]];
            if(b.squared_length() > 1e-6) {
                int a = cellEdges[cell].edges[burgersCircuitEdges[face][0]]->vertex2;
                int b = cellEdges[cell].edges[burgersCircuitEdges[face][1]]->vertex2;
                int c = cellEdges[cell].edges[burgersCircuitEdges[face][2]]->oppositeEdge()->vertex2;
                if(a < atom->nlocal && b < atom->nlocal && c < atom->nlocal) {
                    dislocFaces.push_back(c);
                    dislocFaces.push_back(b);
                    dislocFaces.push_back(a);
                }
            }
        }
    }
    // Gather vertices that need to be written to the output file.
    std::set<int> outputVertices;
    std::vector<int> vertexMapping(vertexEdges.size());
    for(std::vector<int>::const_iterator v = dislocFaces.begin(); v != dislocFaces.end(); ++v) {
        outputVertices.insert(*v);
    }

    // Write VTK output file.
    std::ofstream outputStream(filename.c_str());
    outputStream << "# vtk DataFile Version 3.0\n";
    outputStream << "# Bad tessellation cells\n";
    outputStream << "ASCII\n";
    outputStream << "DATASET UNSTRUCTURED_GRID\n";
    outputStream << "POINTS " << outputVertices.size() << " double\n";
    int index = 0;
    for(std::set<int>::const_iterator v = outputVertices.begin(); v != outputVertices.end(); ++v) {
        outputStream << atom->x[*v][0] << " " << atom->x[*v][1] << " " << atom->x[*v][2] << "\n";
        vertexMapping[*v] = index++;
    }
    int numCells = dislocFaces.size()/3;
    outputStream << "\nCELLS " << numCells << " " << (numCells * 4) << "\n";
    for(std::vector<int>::const_iterator v = dislocFaces.begin(); v != dislocFaces.end(); ) {
        outputStream << "3";
        for(int i = 0; i < 3; i++) {
            outputStream << " " << vertexMapping[*v++];
        }
        outputStream << "\n";
    }
    outputStream << "\nCELL_TYPES " << numCells << "\n";
    for(int i = 0; i < numCells; i++)
        outputStream << "5\n";
}

/*********************************************************************
 * Writes a LAMMPS data file with the vertices and edges of the
 * tessellation.
 *
 * This function is only for debugging purposes.
 *********************************************************************/
void DislocationIdentificationFix::dumpTessellationToDataFile(const std::string& filename)
{
    int natoms = atom->nlocal;
    //natoms += atom->nghost;

    std::vector< std::pair<int,int> > bonds;
    std::vector< int > bondTypes;
//    std::vector< Vector3 > lattice_vectors;
    for(int v = 0; v < (int)natoms; v++) {
        for(Edge* edge = vertexEdges[v]; edge != NULL; edge = edge->nextLeavingEdge) {
            if(edge->latticeVector == -1 || edge->latticeVector == 0) continue;
            if(edge->isOddEdge()) continue;
            int localIndex = edge->vertex2;
            if(comm->nprocs != 1) {
                if(edge->vertex2 >= atom->nlocal) continue;
            }
            else {
                if(edge->vertex2 >= atom->nlocal) {
                    localIndex = atom->map(atom->tag[edge->vertex2]);
                    if(localIndex == -1) continue;
                }
            }
            bonds.push_back(std::make_pair(v, localIndex));
//            bondTypes.push_back(edge->latticeVector);
            bondTypes.push_back(edge->isInitiallyFixed() ? 2 : (edge->isFixed() ? 3 : 1));
//            bondTypes.push_back((edge->latticeVector == edge->deformedLatticeVector) ? 2 : 1);
//            lattice_vectors.push_back(latticeVectors[edge->latticeVector]);
//            lattice_vectors.push_back(-latticeVectors[edge->latticeVector]);
        }
    }
    std::ofstream s(filename.c_str());
    s << "LAMMPS data file\n";
    s << "\n";
    s << natoms << " atoms\n";
    s << bonds.size() << " bonds\n";
    s << "\n";
    int numAtomTypes = structureTypes.empty() ? 1 : (*std::max_element(structureTypes.begin(), structureTypes.end())+1);
    s << numAtomTypes << " atom types\n";
//    s << latticeVectors.size() << " bond types\n";
    s << 3 << " bond types\n";
    s << "\n";
    s << domain->boxlo[0] << " " << domain->boxhi[0] << "xlo xhi\n";
    s << domain->boxlo[1] << " " << domain->boxhi[1] << "ylo yhi\n";
    s << domain->boxlo[2] << " " << domain->boxhi[2] << "zlo zhi\n";
    s << "\nMasses\n\n";
    for(int i = 1; i <= numAtomTypes; i++)
        s << i << " 1\n";
    s << "\nAtoms # bond\n\n";
    for(int i = 0; i < natoms; i++)
        s << atom->tag[i] << " 1 " << (i >= structureTypes.size() ? 1 : (structureTypes[i]+1)) << " " << atom->x[i][0] << " " << atom->x[i][1] << " " << atom->x[i][2] << "\n";
    s << "\nBonds\n\n";
    for(size_t b = 0; b < bonds.size(); b++) {
        s << (b+1) << " " << bondTypes[b] << " " << atom->tag[bonds[b].first] << " " << atom->tag[bonds[b].second] << "\n";
    }
#if 0
    std::ofstream s3("edge_vectors.dat", std::ios_base::out|std::ios_base::binary);
    s3.write(reinterpret_cast<const char*>(lattice_vectors.data()), lattice_vectors.size() * sizeof(Vector3));
#endif
}

/* ----------------------------------------------------------------------
  Find all atoms that are nearest neighbors of the given pair of atoms.
------------------------------------------------------------------------- */
static int findCommonNeighbors(const DislocationIdentificationFix::NeighborBondArray& neighborArray, int neighborIndex, unsigned int& commonNeighbors, int numNeighbors)
{
    commonNeighbors = neighborArray.neighborArray[neighborIndex];

#ifdef __GNUC__
    // Count the number of bits set in neighbor bit-field.
    return __builtin_popcount(commonNeighbors);
#else
    // Count the number of bits set in neighbor bit-field.
    unsigned int v = commonNeighbors - ((commonNeighbors >> 1) & 0x55555555);
    v = (v & 0x33333333) + ((v >> 2) & 0x33333333);
    return (((v + (v >> 4)) & 0xF0F0F0F) * 0x1010101) >> 24;
#endif
}

/* ----------------------------------------------------------------------
   Finds all bonds between common nearest neighbors.
------------------------------------------------------------------------- */
static int findNeighborBonds(const DislocationIdentificationFix::NeighborBondArray& neighborArray, unsigned int commonNeighbors, int numNeighbors, CNAPairBond* neighborBonds)
{
    int numBonds = 0;

    unsigned int nib[DislocationIdentificationFix::NeighborBondArray::CNA_MAX_PATTERN_NEIGHBORS];
    int nibn = 0;
    unsigned int ni1b = 1;
    for(int ni1 = 0; ni1 < numNeighbors; ni1++, ni1b <<= 1) {
        if(commonNeighbors & ni1b) {
            unsigned int b = commonNeighbors & neighborArray.neighborArray[ni1];
            for(int n = 0; n < nibn; n++) {
                if(b & nib[n])
                    neighborBonds[numBonds++] = ni1b | nib[n];
            }

            nib[nibn++] = ni1b;
        }
    }
    return numBonds;
}

/* ----------------------------------------------------------------------
   Find all chains of bonds.
------------------------------------------------------------------------- */
static int getAdjacentBonds(unsigned int atom, CNAPairBond* bondsToProcess, int& numBonds, unsigned int& atomsToProcess, unsigned int& atomsProcessed)
{
    int adjacentBonds = 0;
    for(int b = numBonds - 1; b >= 0; b--) {
        if(atom & *bondsToProcess) {
            ++adjacentBonds;
            atomsToProcess |= *bondsToProcess & (~atomsProcessed);
            memmove(bondsToProcess, bondsToProcess + 1, sizeof(CNAPairBond) * b);
            numBonds--;
        }
        else ++bondsToProcess;
    }
    return adjacentBonds;
}

/* ----------------------------------------------------------------------
   Find all chains of bonds between common neighbors and determine the length
   of the longest continuous chain.
------------------------------------------------------------------------- */
static int calcMaxChainLength(CNAPairBond* neighborBonds, int numBonds)
{
    // Group the common bonds into clusters.
    int maxChainLength = 0;
    while(numBonds) {
        // Make a new cluster, starting with the first remaining bond to be processed.
        numBonds--;
        unsigned int atomsToProcess = neighborBonds[numBonds];
        unsigned int atomsProcessed = 0;
        int clusterSize = 1;
        do {
            // Determine the number of trailing 0-bits in atomsToProcess,
            // starting at the least significant bit position.

#ifdef __GNUC__
            int nextAtomIndex = __builtin_ctz(atomsToProcess);
#else
            // Algorithm is from Bit Twiddling Hacks website.
            static const int MultiplyDeBruijnBitPosition[32] =
            {
              0, 1, 28, 2, 29, 14, 24, 3, 30, 22, 20, 15, 25, 17, 4, 8,
              31, 27, 13, 23, 21, 19, 16, 7, 26, 12, 18, 6, 11, 5, 10, 9
            };
            int nextAtomIndex = MultiplyDeBruijnBitPosition[((unsigned int)((atomsToProcess & -atomsToProcess) * 0x077CB531U)) >> 27];
#endif
            unsigned int nextAtom = 1 << nextAtomIndex;
            atomsProcessed |= nextAtom;
            atomsToProcess &= ~nextAtom;
            clusterSize += getAdjacentBonds(nextAtom, neighborBonds, numBonds, atomsToProcess, atomsProcessed);
        }
        while(atomsToProcess);
        if(clusterSize > maxChainLength)
            maxChainLength = clusterSize;
    }
    return maxChainLength;
}
